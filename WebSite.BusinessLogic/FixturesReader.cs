﻿using System;
using ServicesInterfaces;
using WebSite.Domain.DataTransferObjects;
using System.Collections.Generic;
using WebSite.Respository;
using WebSite.Domain;

namespace WebSite.BusinessLogic
{
    public class FixturesReader : IFixtures<FixturesDTO>
    {
        IFixturesRepository fixturesRepo;

        public FixturesReader()
        {
            fixturesRepo = new LfcFixtures201819();
        }

        public FixturesReader(IFixturesRepository repo)
        {
            if (repo != null)
                fixturesRepo = repo;
            else
                throw new System.Exception();
        }

        public FixturesDTO readAll()
        {
            FixturesDTO dto = new FixturesDTO();
            dto.ListOfFixtures = new List<FixtureDTO>();

            var fixtures = fixturesRepo.ReadAll();

            foreach (var fixture in fixtures)
            {
                FixtureDTO newdto = new FixtureDTO();
                newdto.Opposition = fixture.Opposition;
                newdto.HomeAway = fixture.HomeAway;
                newdto.Schedule = fixture.Schedule;

                dto.ListOfFixtures.Add(newdto);
            }

            dto.TeamName = fixturesRepo.teamName();

            return dto;
        }
    }
}
