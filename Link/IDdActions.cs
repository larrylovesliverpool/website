﻿
// data actions

namespace ServicesInterfaces
{
    public interface IDdActions<dataObject>
    {
        string Create(dataObject data);

        dataObject Retrieve(int key);

        void Update(dataObject obj);

        void Delete(string key);
    }
}
