﻿
// 

using System.Collections.Generic;
using WebSite.Domain.DataTransferObjects;

namespace WebSite.BusinessLogic
{
    public class Fossil
    {
        public FossilsDTO GetAll()
        {
            FossilsDTO fossilDTO = new FossilsDTO();
            fossilDTO.ListOf = new List<FossilDTO>();

            var ListOf = new WebSite.Respository.Fossil().ReadAll();

            foreach (Domain.Fossil fossil in ListOf)
            {
                fossilDTO.ListOf.Add(new FossilDTO() { Name = fossil.Name, Rarity = fossil.Rarity, Comments = fossil.Comments });
            }

            return fossilDTO;
        }
    }
}
