﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebSite.Controllers;
using System.Web.Mvc;

namespace WebSite.UnitTest.Controllers
{
    [TestClass]
    public class ControllerUnitTest
    {
        [TestMethod]
        public void TestMethod_HomeController()
        {
            var controller = new HomeController();
            var result = controller.Calendar() as ViewResult;
            Assert.AreEqual("Details", result.ViewName);
        }
    }
}
