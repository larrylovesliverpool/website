﻿using System.Threading.Tasks;

namespace WebSite.BusinessLogic.HttpClients
{
    public interface IHttpClientServiceCall
    {
        Task<T1> Get<T1, T2>(string url, string baseAddress, string accessKey, T2 payload) where T2 : class;
    }
}