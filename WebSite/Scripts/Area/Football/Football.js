﻿
$(function () {

    // liverpool

    $('#ImgLiverpool').bind('mouseenter', function () {
        $('#PanelLiverpool').removeClass('show').addClass('hidden');
        $('#PanelDetailsLiverpool').removeClass('hidden').addClass('show');

        $('#WebsiteLiverpool').bind('click', function () {

            window.open('http://www.liverpoolfc.com');
        });
    });

    $('#PanelDetailsLiverpool').bind('mouseleave', function () {
        $('#PanelLiverpool').removeClass('hidden').addClass('show');
        $('#PanelDetailsLiverpool').removeClass('show').addClass('hidden');
    });

    // barcelona

    //$('#ImgBarcelona').bind('click', function () {

    //    window.open('https://www.fcbarcelona.com/');
    //});

    $('#ImgBarcelona').bind('mouseenter', function () {

        $('#PanelBarcelona').removeClass('show').addClass('hidden');
        $('#PanelDetailsBarcelona').removeClass('hidden').addClass('show');
    });

    $('#PanelDetailsBarcelona').bind('mouseleave', function () {

        $('#PanelBarcelona').removeClass('hidden').addClass('show');
        $('#PanelDetailsBarcelona').removeClass('show').addClass('hidden');
    });

    // Bayern

    //$('#ImgBayern').bind('click', function () {

    //    window.open('http://www.fcbayern.de/en/');
    //});

    $('#ImgBayern').bind('mouseenter', function () {

        $('#PanelBayern').removeClass('show').addClass('hidden');
        $('#PanelDetailsBayern').removeClass('hidden').addClass('show');
    });

    $('#PanelDetailsBayern').bind('mouseleave', function () {

        $('#PanelBayern').removeClass('hidden').addClass('show');
        $('#PanelDetailsBayern').removeClass('show').addClass('hidden');
    });

    // Juventus

    $('#ImgJuventus').bind('click', function () {

        window.open('http://www.juventus.com/en/');
    });

    $('#ImgJuventus').bind('mouseenter', function () {

        $('#PanelJuventus').removeClass('hidden').addClass('show');
    });

    $('#ImgJuventus').bind('mouseleave', function () {

        $('#PanelJuventus').removeClass('show').addClass('hidden');
    });

});