﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebSite.BusinessLogic;
using WebSite.BusinessLogic.HttpClients.Services;
using WebSite.Domain.Football.MatchDay;
using WebSite.ViewModels;

namespace WebSite.Controllers
{
    public class FootballController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllFixtures()
        {           
            var fixtures = new Fixtures().GetAllFixtures();
            FixturesVM vm = new FixturesVM();
            vm.Fixtures = new List<FixtureVM>();

            foreach (var fixture in fixtures)
            {
                var f = new FixtureVM();

                f.Opposition = fixture.Opposition;
                f.HomeAway = fixture.HomeAway;
                f.Schedule = fixture.Schedule;

                vm.Fixtures.Add(f);
            }

            return View(vm);
        }

        public ActionResult TableStandings()
        {
            StandingsService tableStandings = new StandingsService();
            var crtStandings = tableStandings.GetStandings();

            var crtSeason = tableStandings.GetCurrentSeason();

            return View(crtStandings);
        }

        public ActionResult Squad()
        {
            StandingsService tableStandings = new StandingsService();
            var squad = tableStandings.GetPlayers();

            return View(squad);
        }

        public ActionResult MatchDays()
        {
            FootballService footballService = new FootballService();
            var matchdays = footballService.GetMatchDays();
            List<ViewModels.Football.MatchViewModel> vm = new List<ViewModels.Football.MatchViewModel>();

            /* ****************************************** */
            /* filter result for Liverpool played matches */
            /* ****************************************** */

            var played = matchdays.matches.FindAll(x => x.status == "FINISHED" && ( x.homeTeam.id == 64 || x.awayTeam.id == 64 ) );
            played.Reverse();
            var toPlay = matchdays.matches.FindAll(x => x.status != "FINISHED" && (x.homeTeam.id == 64 || x.awayTeam.id == 64));

            foreach ( var game in played )
            {
                vm.Add(new ViewModels.Football.MatchViewModel
                {
                    id = game.id,
                    matchday = game.matchday,
                    score = game.score,
                    homeTeam = game.homeTeam,
                    awayTeam = game.awayTeam,
                    utcDate = game.utcDate
                });
            }

            return View(vm);
        }

        public ActionResult Fixtures()
        {
            FootballService footballService = new FootballService();
            var matchdays = footballService.GetMatchDays();
            List<ViewModels.Football.MatchViewModel> vm = new List<ViewModels.Football.MatchViewModel>();

            /* ****************************************** */
            /* filter result for Liverpool played matches */
            /* ****************************************** */

            var toPlay = matchdays.matches.FindAll(x => x.status != "FINISHED" && (x.homeTeam.id == 64 || x.awayTeam.id == 64));

            foreach (var game in toPlay)
            {
                vm.Add(new ViewModels.Football.MatchViewModel
                {
                    id = game.id,
                    matchday = game.matchday,
                    score = game.score,
                    homeTeam = game.homeTeam,
                    awayTeam = game.awayTeam,
                    utcDate = game.utcDate
                });
            }

            return View(vm);
        }

    }
}