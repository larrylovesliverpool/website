﻿using AutoMapper;

using WebSite.ViewModels;
using WebSite.Domain.dto;
using WebSite.Domain.BaseModels;
using System.Collections.Generic;

namespace WebSite
{
    public class AutoMapperConfig
    {
        public static void RegisterMappers()
        {
            // *************************
            // mappings between DM & VM.
            // *************************

            Mapper.Initialize(cfg => {
                cfg.CreateMap<Domain.DccDecoder, DccDecoderVM>();
                cfg.CreateMap<LatestBookMark, VM_LatestBookMark>();
                cfg.CreateMap<BookMarksModel, BookmarksVM>();
            });

        }
    }
}