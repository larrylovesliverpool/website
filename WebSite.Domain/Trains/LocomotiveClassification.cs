﻿using System.ComponentModel.DataAnnotations;

namespace WebSite.Domain.Railways
{
    public class LocomotiveClassification
    {
        private int _classificationId;
        private string _power;
        private string _classification;
        private string _comments;

        [Key]
        public int ClassificationId
        {
            get
            {
                return _classificationId;
            }

            set
            {
                _classificationId = value;
            }
        }

        public string Power
        {
            get
            {
                return _power;
            }

            set
            {
                _power = value;
            }
        }

        public string Classification
        {
            get
            {
                return _classification;
            }

            set
            {
                _classification = value;
            }
        }

        public string Comments
        {
            get
            {
                return _comments;
            }

            set
            {
                _comments = value;
            }
        }
    }
}
