﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Areas.Development.Controllers
{
    public class BootstrapController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Bootstrap()
        {
            return View();
        }
    }
}