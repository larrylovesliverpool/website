﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Link;
using WebSite.Domain.DataTransferObjects;
using WebSite.Respository;

namespace WebSite.BusinessLogic
{
    public class HomePageFixtures : IHomePagefixtures
    {
        public HomePageFixtureDTO GetFixtures()
        {
            HomePageFixtureDTO fixtures = new HomePageFixtureDTO();

            var LfcFixture = new Fixtures(new LfcFixtures201920()).GetNextFixtures();
            var BarFixture = new Fixtures(new BarFixtures201819()).GetNextFixtures();

            fixtures.LfcFixture = LfcFixture;
            fixtures.BarFixture = BarFixture;

            return fixtures;
        }
    }
}
