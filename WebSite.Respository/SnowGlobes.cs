﻿using System;
using System.Collections.Generic;
using WebSite.Domain;

namespace WebSite.Respository
{
    public class SnowGlobes
    {
        public List<SnowGlobe> ReadAll()
        {
            List<SnowGlobe> ListOfSnowGlobes = new List<SnowGlobe>();

            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "City Skyline", Rarity = "Common", Comments = "" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "Haunted Mansion", Rarity = "Common", Comments = "" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "Newcrest", Rarity = "Common", Comments = "" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "The City Bridge", Rarity = "Common", Comments = "Romance Festival" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "The Willow Creek Lighthouse", Rarity = "Common", Comments = "" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "Tudor House Snow Globe", Rarity = "Common", Comments = "" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "Yuma Heights", Rarity = "Common", Comments = "" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "Emily", Rarity = "Uncommon", Comments = "GeekCon" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "Freezer Bunny", Rarity = "Uncommon", Comments = "Spice Festival" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "Stanley the Jackalope", Rarity = "Uncommon", Comments = "" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "The City Skyscrapper", Rarity = "Uncommon", Comments = "Humour and Hijinks Festival" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "Age of the Dinosaur", Rarity = "Rare", Comments = "" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "Plumbob (Limited Edition)", Rarity = "Rare", Comments = "" });
            ListOfSnowGlobes.Add(new SnowGlobe() { Name = "The Reaper (Limited Edition)", Rarity = "Rare", Comments = "" });

            return ListOfSnowGlobes;
        }
    }
}
