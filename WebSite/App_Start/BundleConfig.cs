﻿using System.Web;
using System.Web.Optimization;

namespace WebSite
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/Home").Include(
                    "~/Scripts/Home/HomePage.js"));

            bundles.Add(new ScriptBundle("~/bundles/ToolTip").Include(
                    "~/Scripts/jquery.qtip.js"));

            bundles.Add(new ScriptBundle("~/bundles/ToolTipCSS").Include(
                    "~/Scripts/jquery.qtip.css.js"));

            bundles.Add(new StyleBundle("~/Content/DatatablesCSS").Include(
                        "~/Content/DataTables/css/buttons.bootstrap.css",
                        "~/Content/DataTables/css/dataTables.bootstrap.css"));

            bundles.Add(new ScriptBundle("~/bundles/DataTables").Include(
                        "~/Scripts/DataTables/jquery.dataTables.js",
                        "~/Scripts/DataTables/dataTables.bootstrap.min.js"));

        }
    }
}
