﻿using ServicesInterfaces;
using System.Collections.Generic;
using WebSite.Domain;

namespace WebSite.Respository
{
    public class F1Races2018 : IF1RacesRepository
    {
        private string _season = "2018";

        public string Season
        {
            get
            {
                return _season;
            }

            set
            {
                _season = value;
            }
        }

        public List<F1Race> Read()
        {
            var races = new List<F1Race>
            {
                new F1Race() { race = "Australian", location = "Albert Park", dateOfweekend = "13-15 March", dateOfRace = new System.DateTime(2020,03,15) },
                new F1Race() { race = "Bahrain", location = "Bahrain International Circuit", dateOfweekend = "29-31 March", dateOfRace = new System.DateTime(2020,03,31) },
                new F1Race() { race = "Chinese", location = "Shanghai International Circuit", dateOfweekend = "13-15 April", dateOfRace = new System.DateTime(2020,04,15) },
                new F1Race() { race = "Azerbaijan", location = "Baku City Circuit", dateOfweekend = "27-29 April", dateOfRace = new System.DateTime(2020,04,29) },
                new F1Race() { race = "Spanish", location = "Circuit de Catalunya", dateOfweekend = "11-13 May", dateOfRace = new System.DateTime(2019,05,13) },
                new F1Race() { race = "Monaco", location = "Monaco", dateOfweekend = "24-27 May", dateOfRace = new System.DateTime(2019,05,27) },
                new F1Race() { race = "Canadian", location = "Circuit Giles Villeneuve", dateOfweekend = "8-10 June", dateOfRace = new System.DateTime(2019,06,10) },
                new F1Race() { race = "French", location = "Paul Ricard", dateOfweekend = "22-24 June", dateOfRace = new System.DateTime(2019,06,24) },
                new F1Race() { race = "Austrian", location = "Red Bull Ring", dateOfweekend = "29 June - 1 July", dateOfRace = new System.DateTime(2019,07,1) },
                new F1Race() { race = "British", location = "Silverstone", dateOfweekend = "12-14 July", dateOfRace = new System.DateTime(2019,07,14) },
                new F1Race() { race = "German", location = "Hockenheimring", dateOfweekend = "27-28 July", dateOfRace = new System.DateTime(2019,07,28) },
                new F1Race() { race = "Hungarian", location = "Hungaroring", dateOfweekend = "3-4 August", dateOfRace = new System.DateTime(2019,08,4) },
                new F1Race() { race = "Belgian", location = "Spa-Francorchamps", dateOfweekend = "24-26 August", dateOfRace = new System.DateTime(2019,08,26) },
                new F1Race() { race = "Italian", location = "Monza", dateOfweekend = "31 August - 2 September", dateOfRace = new System.DateTime(2019,09,02) },
                new F1Race() { race = "Singapore", location = "Marina Bay", dateOfweekend = "14-16 September", dateOfRace = new System.DateTime(2019,09,16) },
                new F1Race() { race = "Russian", location = "Sochi", dateOfweekend = "28-30 September", dateOfRace = new System.DateTime(2019,09,30) },
                new F1Race() { race = "Japanese", location = "Suzuka", dateOfweekend = "5-7 October", dateOfRace = new System.DateTime(2019,10,7) },
                new F1Race() { race = "United States", location = "Austin", dateOfweekend = "19-21 October", dateOfRace = new System.DateTime(2019,10,21) },
                new F1Race() { race = "Mexican", location = "Mexico City", dateOfweekend = "26-28 October", dateOfRace = new System.DateTime(2019,10,28) },
                new F1Race() { race = "Brazilian", location = "Sao Paulo", dateOfweekend = "9-11 November", dateOfRace = new System.DateTime(2019,11,11) },
                new F1Race() { race = "Abu Dhabi", location = "Yas Marina", dateOfweekend = "29-01 December", dateOfRace = new System.DateTime(2019,11,25) },
                new F1Race() { race = "Season Finished", location = "2020", dateOfweekend = "New Season Starts March 2020", dateOfRace = new System.DateTime(2020,03,01) }
            };

            return races;
        }

    }
}
