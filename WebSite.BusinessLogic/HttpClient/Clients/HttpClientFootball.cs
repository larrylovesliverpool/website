﻿using System;
using WebSite.BusinessLogic.HttpClients;
using WebSite.BusinessLogic.HttpClients.Callers;

namespace WebSite.BusinessLogic.HttpClients.Clients
{
    public class HttpClientFootball
    {

        private IHttpClientServiceCall _httpClientServiceCall;
        private readonly string _name = "Football Client";

        public IHttpClientServiceCall HttpClientServiceCall
        {
            get { return _httpClientServiceCall; }
            set { _httpClientServiceCall = value; }
        }

        public IHttpClientServiceCall Caller
        {
            get { return _httpClientServiceCall; }
            set { _httpClientServiceCall = value; }
        }

        public string Name
        {
            get { return _name; }
        }

        private static readonly Lazy<HttpClientFootball> lazy = new Lazy<HttpClientFootball>(() => new HttpClientFootball());

        private HttpClientFootball()
        {
            HttpClientServiceCall = new HttpClientFootballCalls();
        }

        public static HttpClientFootball Service => lazy.Value;
    }

}
