﻿using System;
using System.Web.Mvc;

namespace WebSite.Helper.Filter
{
    public class Authorization : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            filterContext.HttpContext.Response.Write("<div>Authorization</div>");
        }
    }
}