﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain.DataTransferObjects
{
    public class TrainCoachDTO : TrainsRollingStockDTO
    {
        private int _coachID;
        private int _coachType;
        private string _manufacturerName;

        public string ManufacturerName
        {
            get
            {
                return _manufacturerName;
            }

            set
            {
                _manufacturerName = value;
            }
        }
        public int CoachType
        {
            get
            {
                return _coachType;
            }

            set
            {
                _coachType = value;
            }
        }
        public int CoachID
        {
            get
            {
                return _coachID;
            }

            set
            {
                _coachID = value;
            }
        }
    }
}
