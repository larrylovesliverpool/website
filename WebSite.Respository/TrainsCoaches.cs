﻿using System.Collections.Generic;
using System.Linq;
using WebSite.Domain;
using System.Data.SqlClient;

namespace WebSite.Repository
{
    public class TrainCoachesRepository : WebSite.ServiceInterfaces.ITrainsCoachesRepository
    {
        List<TrainsCoach> coaches = new List<TrainsCoach>
        {
            new TrainsCoach() {  CoachID=1, ManufacturerID = 1, Description="BR MK2 (Blue & Grey) Inter City 2nd",Quantity=2 },
            new TrainsCoach() {  CoachID=2, ManufacturerID = 1, Description="BR MK2 (Blue & Grey) Inter City 2nd Brake",Quantity=1 },
            new TrainsCoach() {  CoachID=3, ManufacturerID = 1, Description="BR MK2 (Blue & Grey) Inter City 2nd Buffet",Quantity=1 },
            new TrainsCoach() {  CoachID=4, ManufacturerID = 1, Description="LNER Teak Composite ",Quantity=2 },
            new TrainsCoach() {  CoachID=5, ManufacturerID = 1, Description="LNER Teak Brake Composite",Quantity=1 },
            new TrainsCoach() {  CoachID=6, ManufacturerID = 1, Description="LNER Teak Buffet",Quantity=1 },
            new TrainsCoach() {  CoachID=7, ManufacturerID = 1, Description="BR MK1 (Crimson and Cream) Composite",Quantity=2 },
            new TrainsCoach() {  CoachID=8, ManufacturerID = 1, Description="BR MK1 (Crimson and Cream) Brake Composite",Quantity=1 },
            new TrainsCoach() {  CoachID=9, ManufacturerID = 1, Description="Virgin Mk2d",Quantity=1 },
            new TrainsCoach() { CoachID=10, ManufacturerID = 1, Description="Virgin Mk2d Brake",Quantity=1 },
            new TrainsCoach() { CoachID=11, ManufacturerID = 1, Description="BR Parcel van (maroon)",Quantity=2 },
            new TrainsCoach() { CoachID=12, ManufacturerID = 1, Description="BR MK1 (Maroon) Composite, Talisman",Quantity=2 },
            new TrainsCoach() { CoachID=13, ManufacturerID = 1, Description="BR MK1 (Maroon) Brake Composite, Talisman",Quantity=1 },
            new TrainsCoach() { CoachID=14, ManufacturerID = 1, Description="BR MK1 (Chocolate and Cream) Composite",Quantity=2 },
            new TrainsCoach() { CoachID=15, ManufacturerID = 1, Description="BR MK1 (Chocolate and Cream) Brake Composite",Quantity=1 },
        };

        public List<TrainsCoach> Retrieve() => coaches;

        public TrainsCoach Retrieve(int key) => coaches.Find(i => i.CoachID == key);

        public List<TrainsCoach> Retrieve(string searchText) => coaches.Where(i => i.Description.Contains(searchText)).ToList();

        public int total() => coaches.Sum(i => i.Quantity);
    }
}
