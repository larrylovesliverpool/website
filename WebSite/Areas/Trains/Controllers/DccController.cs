﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.ViewModels;
using AutoMapper;

namespace WebSite.Areas.Trains.Controllers
{
    public class DccController : Controller
    {
        [HandleError]
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult DecoderDescription()
        {
            return View();
        }

        public ActionResult StayALive() => View();

        public ActionResult ElectroFrog() => View();

        public ActionResult DccDecoderDetails(int identifier) => View(new DccDecoderVM().Generate(identifier));

    }
}