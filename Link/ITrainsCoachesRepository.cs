﻿using WebSite.Domain;

namespace WebSite.ServiceInterfaces
{
    public interface ITrainsCoachesRepository : IRepositoryReader<TrainsCoach> {}
}
