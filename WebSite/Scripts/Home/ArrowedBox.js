﻿

// JavaScript source code
// Arrowed box
// Copyright DDM Solutions 2017

jQuery.fn.ArrowBox = function (paramaters) {

    this.addClass('arrow_box');
    var arrowedBox = this;

    // things ToDo ...

    // a. Colours for border, header txt, txt
    // b. Data source for contents.

    // style commands
    this.css('position', 'absolute');
    this.css('display', 'inline');
    this.css('top', '-30px');
    this.css('left', '75px');
    this.css('z-index', '999');

    // height and width of box
    this.css('width', '100px');

    if (paramaters.width != null) {
        this.css('width', paramaters.width);
    } else if( this.attr('data-width')) {
        this.css('width', this.attr('data-width'));
    }

    this.css('min-height', '100px');

    if (paramaters.height != null) {
        this.css('min-height', paramaters.height);
    } else if (this.attr('data-height')) {
        this.css('min-height', this.attr('data-height'));
    }

    // Header content
    var headerTxt = '<div style="text-align:center; border-top-left-radius:4px; border-top-right-radius:4px; background-color:#F8F8F8 ;margin:0px;padding:2px;">';

    if (paramaters.header != null) {
        headerTxt += paramaters.header;
    } else {
        headerTxt += this.attr('data-header');
    }

    headerTxt += '</div>';

    this.html(headerTxt);

    // Main body content
    if (paramaters.contents != null) {
        this.append(paramaters.contents);
    }

    else if (this.attr('data-contents') != null) {
        this.append(this.attr('data-contents'));
    }

    // Close Button
    if (paramaters.close || this.attr('data-close') == 'True')
    {
        var closeBtn = '<div id="ArrowedBoxClose" style="text-align:right;position:absolute;bottom:0;right:0;"><span style=" font-size:x-small; border-top-left-radius:4px;  border-bottom-right-radius:4px; background-color:orange;  margin:0px;padding:2px;">Close</span></div>';
        this.append(closeBtn);

    }

    return (arrowedBox);
}

/* EOF */
