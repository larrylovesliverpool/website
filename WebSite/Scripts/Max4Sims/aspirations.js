﻿
/* aspirations randon generator */

function generateAspiration()
{
    var aspirations = [ "animal - friend of the animals",
                        "athletic - body builder",
                        "creativity - painter extraordinaire",
                        "creativity - musical genius",
                        "creativity - bestselling author",
                        "deviance - public enemy",
                        "deviance - chief of mischief",
                        "family - super parent",
                        "family - successful lineage",
                        "family - big happy family",
                        "food - master cheif",
                        "food - master mixologist",
                        "fortune - fabulously wealthy",
                        "fortune - mansion baron",
                        "knowledge - renaissance sim",
                        "knowledge - archaeology scholar",
                        "knowledge - nerd brain",
                        "knowledge - computer whiz",
                        "location - city native",
                        "love - serial romantic",
                        "love - soul mate",
                        "nature - angling ace",
                        "nature - freelance botanist",
                        "nature - the curator",
                        "nature - outdoor enthusiast",
                        "nature - jungle explorer",
                        "popularity - leader of the pack",
                        "popularity - joke star",
                        "popularity - party animal",
                        "popularity - friend of the world"];

    return aspirations[Math.floor(Math.random()*30)];

}

function generateTrait()
{
    /* obj to store trait number and text */
    var obj = { "Number": 0, "Text": "" };

    var trait = [   "active",
                    "cheerful",
                    "creative",
                    "genius",
                    "gloomy",
                    "goofball",
                    "hot-headed",
                    "romantic",
                    "self-assured",
                    "unflirtly",
                    "art lover",
                    "bookworm",
                    "foodie",
                    "geek",
                    "music lover",
                    "perfectionist",
                    "cat lover",
                    "dog lover",
                    "ambitious",
                    "childish",
                    "clumsy",
                    "dance machine",
                    "glutton",
                    "insane",
                    "kleptomaniac",
                    "lazy",
                    "loves outdoors",
                    "materialistic",
                    "neat",
                    "slob",
                    "snob",
                    "squeamish",
                    "Vegetarian",
                    "bro",
                    "evil",
                    "family-oriented",
                    "good",
                    "hates children",
                    "insider",
                    "jealous",
                    "loner",
                    "mean",
                    "noncommittal",
                    "outgoing"];

    obj.Number = Math.floor(Math.random() * 44);
    obj.Text = trait[obj.Number];

    return obj;
}

function generateToddlerTrait() {

    /* obj to store trait number and text */
    var obj = { "Number": 0, "Text": "" };

    var trait = ["Angelic",
                "Charmer",
                "Clingy",
                "Fussy",
                "Independent",
                "Inquisitive",
                "Silly",
                "Wild"];

    obj.Number = Math.floor(Math.random() * 8);
    obj.Text = trait[obj.Number];

    return obj;
}

function generateChildTrait() {

    var obj = { "Number": 0, "Text": "" };

    var childtraits = [
		            "active",
                    "cheerful",
                    "creative",
                    "genius",
                    "gloomy",
                    "goofball",
                    "hot-headed",
                    "self-assured",
                    "art lover",
                    "bookworm",
                    "geek",
                    "music lover",
                    "perfectionist",
                    "cat lover",
                    "dog lover",
                    "glutton",
                    "insane",
                    "kleptomaniac",
                    "lazy",
                    "loves outdoors",
                    "neat",
                    "slob",
                    "squeamish",
                    "Vegetarian",
                    "evil",
                    "good",
                    "insider",
                    "loner",
                    "mean",
                    "outgoing"];

    obj.Number = Math.floor(Math.random() * 30);
    obj.Text = childtraits[obj.Number];

    return obj;
}

function generateTeenTrait() {

    var obj = { "Number": 0, "Text": "" };

    var teentraits = [
                "active",
                "cheerful",
                "creative",
                "genius",
                "gloomy",
                "goofball",
                "hot-headed",
                "self-assured",
                "unflirtly",
                "art lover",
                "bookworm",
                "foodie",
                "geek",
                "music lover",
                "perfectionist",
                "cat lover",
                "dog lover",
                "childish",
                "clumsy",
                "dance machine",
                "glutton",
                "insane",
                "kleptomaniac",
                "lazy",
                "loves outdoors",
                "materialistic",
                "neat",
                "slob",
                "snob",
                "squeamish",
                "Vegetarian",
                "bro",
                "evil",
                "good",
                "hates children",
                "insider",
                "jealous",
                "loner",
                "mean",
                "outgoing"];

    obj.Number = Math.floor(Math.random() * 40);
    obj.Text = teentraits[obj.Number];

    return obj;
}

function generateTraits() {

    var traits = [generateChildTrait(), generateTeenTrait(), generateTrait()];

    /* test for repeat */

    while (traits[0].Text === traits[1].Text ||
             traits[0].Text === traits[2].Text ||
                traits[1].Text === traits[2].Text)
    {
        /* repeat generate */
        traits = [generateChildTrait(), generateTeenTrait(), generateTrait()];
    }

    return traits;
}

/* child aspiration */
function generateChildAspiration()
{
    var aspiration = ["Artistic Prodigy (Creative)",
                      "Rambunctious Scamp (Fitness)",
                      "Social Butterfly (Social)",
                      "Whiz Kid (Mental)"];

    return aspiration[Math.floor(Math.random()*4)];
}