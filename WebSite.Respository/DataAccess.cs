﻿using WebSite.Repository;

namespace WebSite.Repository
{
    public class DataAccess : ConnectionManager
    {
        public DataAccess(string name) : base(name)
        {
        }

        // calls to actually perform db tasks
        //
        // ie executequery
    }
}
