﻿
using System.Web.Mvc;
using System.Collections.Generic;

using WebSite.ViewModels;
using WebSite.Domain;
using WebSite.Helper.Filter;
using WebSite.BusinessLogic;
using WebSite.BusinessLogic.HttpClients.Services;
using WebSite.Helper.View;

namespace WebSite.Controllers
{
    public class HomeController : Controller
    {
        public HomeController() { }

        // *********************************************
        // Domain model --> DTO model     --> View Model
        // Repository   --> Service Layer --> UI Layer
        // *********************************************

        [FooterMessage]
        [HandleError]
        public ActionResult Index()
        {
            ViewBag.ShowLatest = false;

            var f1Races = new F1Races();
            var nextRace = f1Races.NextRace();

            var fixtures = new HomePageFixtures().GetFixtures();

            HomePageVM homepageVM = new HomePageVM();

            return View(homepageVM.Generate(nextRace, fixtures));

        }

        public ActionResult About() => View();

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Bookmarks()
        {
            //BookmarksVM vm = AutoMapper.Mapper.Map<BookmarksVM>(BookMarks.Read());

            ViewBag.ShowLatest = false;
            ViewBag.Show = "Main";

            return View();
        }

        public ActionResult Calendar() {
            return View("Calendar");
        }

        public ActionResult Error() {

            return View();
        }

    }
}