﻿using ServicesInterfaces;
using System.Collections.Generic;
using WebSite.Domain.DataTransferObjects;
using WebSite.Respository;

namespace WebSite.BusinessLogic
{
    public class Teams
    {
        IFixturesRepository lfcRepo = new LfcFixtures201819();
        IFixturesRepository barRepo = new BarFixtures201718();

        public Teams()
        {

        }

        public Teams(IFixturesRepository lfcParam, IFixturesRepository barParam)
        {
            lfcRepo = lfcParam;
            barRepo = barParam;
        }

        public TeamsFixturesDTO readTeamsFixtures()
        {
            TeamsFixturesDTO data = new TeamsFixturesDTO();

            var lfc = new FixturesReader(lfcRepo).readAll();
            var bar = new FixturesReader(barRepo).readAll();

            data.ListOfTeamsFixtures = new List<FixturesDTO>();

            data.ListOfTeamsFixtures.Add(lfc);
            data.ListOfTeamsFixtures.Add(bar);

            return data;
        }

    }
}
