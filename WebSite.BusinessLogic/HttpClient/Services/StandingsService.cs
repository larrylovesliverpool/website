﻿using System.Configuration;
using System.Threading.Tasks;
using WebSite.Domain;
using WebSite.Domain.Football.Season;
using WebSite.Domain.Football.Team;

namespace WebSite.BusinessLogic.HttpClients.Services
{
    public class StandingsService
    {
        private string _baseAddress;
        private string _securityToken;

        public string BaseAddress
        {
            get => _baseAddress;
            set => _baseAddress = value;
        }

        public string SecurityToken
        {
            get => _securityToken;
            set => _securityToken = value;
        }

        public StandingsService()
        {
            BaseAddress = ConfigurationManager.AppSettings["footballAPI"].ToString();
            SecurityToken = ConfigurationManager.AppSettings["X-Auth-Token"].ToString();
        }

        #region standings

        public TableStandings GetStandings()
        {
            return Task_GetStandings().Result;
        }

        private async Task<TableStandings> Task_GetStandings()
        {
            /* ************************* */
            /* async call to user client */
            /* ************************* */

            var serviceurl = $"competitions/2021/standings";
            string payload = null;

            return await Task.Run(() => {
                return HttpClientTableStandings.Service.Caller.Get<TableStandings, string>(serviceurl, BaseAddress, SecurityToken, payload).Result;
            }).ConfigureAwait(continueOnCapturedContext: false);
        }

        #endregion

        #region team

        public TeamMembers GetPlayers()
        {
            return Task_Getplayers().Result;
        }

        private async Task<TeamMembers> Task_Getplayers()
        {
            var serviceurl = $"teams/64";
            string payload = null;

            return await Task.Run(() => {
                return HttpClientTableStandings.Service.Caller.Get<TeamMembers, string>(serviceurl, BaseAddress, SecurityToken, payload).Result;
            }).ConfigureAwait(continueOnCapturedContext: false);
        }

        #endregion

        #region currentSeason

        /* Extract Current season details */

        public SeasonDetails GetCurrentSeason()
        {
            /* return subset of data */
            return Task_GetCurrentSeason().Result;
        }

        public SeasonDetails GetAllSeasons()
        {
            /* return all details */
            return Task_GetCurrentSeason().Result;
        }

        private async Task<SeasonDetails> Task_GetCurrentSeason()
        {
            /* ************************* */
            /* async call to user client */
            /* ************************* */

            var serviceurl = $"competitions/2021";
            string payload = null;

            return await Task.Run(() => {
                return HttpClientTableStandings.Service.Caller.Get<SeasonDetails, string>(serviceurl, BaseAddress, SecurityToken, payload).Result;
            }).ConfigureAwait(continueOnCapturedContext: false);
        }

        #endregion
    }

}
