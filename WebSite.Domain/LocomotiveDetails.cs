﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class LocomotiveDetails
    {
        private int _locoId;
        private string _locomotiveName;
        private string _description;
        private int _type;
        private string _locomotiveType;
        private int _madeBy;
        private string _locomotiveManufacturer;
        private int _yearPurchased;
        private int _dccValue;
        private int _decoder;
        private string _decoderManufacturer;
        private string _decoderModel;

        public int LocoId
        {
            get
            {
                return _locoId;
            }

            set
            {
                _locoId = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public int Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
            }
        }

        public string LocomotiveType
        {
            get
            {
                return _locomotiveType;
            }

            set
            {
                _locomotiveType = value;
            }
        }

        public int MadeBy
        {
            get
            {
                return _madeBy;
            }

            set
            {
                _madeBy = value;
            }
        }

        public string LocomotiveManufacturer
        {
            get
            {
                return _locomotiveManufacturer;
            }

            set
            {
                _locomotiveManufacturer = value;
            }
        }

        public int YearPurchased
        {
            get
            {
                return _yearPurchased;
            }

            set
            {
                _yearPurchased = value;
            }
        }

        public int DccValue
        {
            get
            {
                return _dccValue;
            }

            set
            {
                _dccValue = value;
            }
        }

        public int Decoder
        {
            get
            {
                return _decoder;
            }

            set
            {
                _decoder = value;
            }
        }

        public string DecoderManufacturer
        {
            get
            {
                return _decoderManufacturer;
            }

            set
            {
                _decoderManufacturer = value;
            }
        }

        public string DecoderModel
        {
            get
            {
                return _decoderModel;
            }

            set
            {
                _decoderModel = value;
            }
        }

        public string LocomotiveName
        {
            get
            {
                return _locomotiveName;
            }

            set
            {
                _locomotiveName = value;
            }
        }
    }
}
