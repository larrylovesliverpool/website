﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain.BaseModels
{
    public class BookMark
    {
        private string _title;
        private string _url;
        private string _category;
        private DateTime _lastAccess;

        public string Title
        {
            get
            {
                return _title;
            }

            set
            {
                _title = value;
            }
        }

        public string Url
        {
            get
            {
                return _url;
            }

            set
            {
                _url = value;
            }
        }

        public DateTime LastAccess
        {
            get
            {
                return _lastAccess;
            }

            set
            {
                _lastAccess = value;
            }
        }

        public string Category
        {
            get
            {
                return _category;
            }

            set
            {
                _category = value;
            }
        }
    }
}
