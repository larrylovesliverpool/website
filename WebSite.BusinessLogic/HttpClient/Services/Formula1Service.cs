﻿using System.Configuration;
using System.Threading.Tasks;
using WebSite.BusinessLogic.HttpClients.Clients;
using WebSite.Domain.Football.MatchDay;
using WebSite.Domain.Formula1;

namespace WebSite.BusinessLogic.HttpClients.Services
{
    public class Formula1Service
    {
        private string _baseAddress;
        private string _securityToken;

        public string BaseAddress
        {
            get => _baseAddress;
            set => _baseAddress = value;
        }
        public string SecurityToken { get => _securityToken; set => _securityToken = value; }

        public Formula1Service()
        {
            BaseAddress = ConfigurationManager.AppSettings["formula1API"].ToString();
        }

        #region team

        public Formula1Races GetRaces()
        {
            return Task_GetRaces().Result;
        }

        private async Task<Formula1Races> Task_GetRaces()
        {
            var serviceurl = $"2020.json";
            string payload = null;

            return await Task.Run(() => {
                return HttpClientFormula1.Service.Caller.Get<Formula1Races, string>(serviceurl, BaseAddress, SecurityToken,payload).Result;
            }).ConfigureAwait(continueOnCapturedContext: false);
        }

        #endregion
    }
}
