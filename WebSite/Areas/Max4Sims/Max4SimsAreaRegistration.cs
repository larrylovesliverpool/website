﻿using System.Web.Mvc;

namespace WebSite.Areas.Max4Sims
{
    public class Max4SimsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Max4Sims";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Max4Sims_default",
                "Max4Sims/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}