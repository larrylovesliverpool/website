﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.BusinessLogic
{
    public interface ITrainWagons<dataObject>
    {
        List<dataObject> read();

        dataObject read(int key);
    }
}
