﻿using WebSite.Domain;

namespace WebSite.ServiceInterfaces
{
    public interface ITrainsManufacturerRepository : IRepositoryReader<TrainsManufacturer> {}
}
