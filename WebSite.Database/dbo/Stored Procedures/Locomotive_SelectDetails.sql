﻿CREATE PROCEDURE [dbo].[Locomotive_SelectDetails]
	@locoId int
AS

	SELECT [LocoId]
      ,L.[Name]
      ,[Type]
      ,[MadeBy]
      ,[Decoder]
      ,[DCCValue]
      ,[Description]
      ,[YearPurchased]
	FROM [dbo].[Locomotives] L
	LEFT JOIN  Manufacturer M ON L.MadeBy = M.ManufacturerId

RETURN 0
