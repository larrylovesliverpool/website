﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class BaseModel
    {
        private bool _hasData;

        public bool hasData
        {
            get
            {
                return _hasData;
            }

            set
            {
                _hasData = value;
            }
        }
    }
}
