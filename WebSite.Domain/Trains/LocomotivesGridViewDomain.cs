﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain.Trains
{
    public class LocomotivesGridViewDomain
    {
        private int _locoId;
		private string _name;
		private string _description;
		private string _manufacturer;
		private int _DCCId;

        public int LocoId { get => _locoId; set => _locoId = value; }
        public string Name { get => _name; set => _name = value; }
        public string Description { get => _description; set => _description = value; }
        public string Manufacturer { get => _manufacturer; set => _manufacturer = value; }
        public int DCCId { get => _DCCId; set => _DCCId = value; }
    }
}
