﻿using System.Collections.Generic;
using WebSite.Domain;

namespace ServicesInterfaces
{
    public interface IFixturesRepository
    {
        IList<fixture> ReadAll();

        fixture read(int identifier);

        string teamName();
    }
}
