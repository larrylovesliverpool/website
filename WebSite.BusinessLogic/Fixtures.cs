﻿using ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Domain.DataTransferObjects;
using WebSite.Respository;

namespace WebSite.BusinessLogic
{
    public class Fixtures : IFixtures
    {
        IFixturesRepository fixturesRepo;

        public Fixtures()
        {
            fixturesRepo = new LfcFixtures201819();
        }

        public Fixtures(IFixturesRepository fixturesParam)
        {
            fixturesRepo = fixturesParam;
        }

        public FixtureDTO GetHomeFixtures()
        {
            var lfc = new FixturesReader(fixturesRepo).readAll();

            var nextFixture = lfc.ListOfFixtures.LastOrDefault(x => x.HomeAway == "Home");

            return nextFixture;
        }

        public FixtureDTO GetAwayFixtures()
        {
            var lfc = new FixturesReader(fixturesRepo).readAll();

            var nextFixture = lfc.ListOfFixtures.LastOrDefault(x => x.HomeAway == "Away");

            return nextFixture;
        }

        public FixtureDTO GetNextFixtures()
        {
            var lfc = new FixturesReader(fixturesRepo).readAll();
            FixtureDTO nextFixture = new FixtureDTO();

            if (lfc.ListOfFixtures.Count(x => x.Schedule > DateTime.Now) >= 1)
            {
                nextFixture = lfc.ListOfFixtures.FirstOrDefault(x => x.Schedule > DateTime.Now);
                nextFixture.HasFixture = true;
            }
            else
            {
                nextFixture.HasFixture = false;
            }

            return nextFixture;
        }

        public IList<FixtureDTO> GetAllFixtures()
        {
            IList<FixtureDTO> ListOf = new List<FixtureDTO>();
            var allFixtures = new FixturesReader(fixturesRepo).readAll();

            ListOf = allFixtures.ListOfFixtures.ToList();

            return ListOf;
        }

    }
}
