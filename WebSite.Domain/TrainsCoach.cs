﻿

namespace WebSite.Domain
{
    public class TrainsCoach : TrainsRollingStock
    {
        private int _coachID;
        
        public int CoachID
        {
            get
            {
                return _coachID;
            }

            set
            {
                _coachID = value;
            }
        }

    }
}
