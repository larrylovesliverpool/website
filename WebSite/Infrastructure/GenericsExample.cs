﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Infrastructure
{
    public class GenericsExample<T> : IGenericExample
    {
        private T _model;

        public T Model
        {
            get { return _model; }
            set { _model = value; }
        }

        #region "Constructors"

        public GenericsExample() { }

        public GenericsExample(T t) {
            Model = t;
        }

        #endregion

        #region "Methods"

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Add(T node )
        {
            Model = node;
        }

        public void Remove()
        {
            throw new NotImplementedException();
        }

        public void Sort()
        {
            throw new NotImplementedException();
        }

        public void Shuffle()
        {
            throw new NotImplementedException();
        }

        #endregion

        public int count
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }

    public class GenericExampleList<T>
    {
        private T _model;

        public T Model
        {
            get { return _model; }
            set { _model = value; }
        }
    }
}