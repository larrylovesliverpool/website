﻿

namespace WebSite.Domain.DataTransferObjects
{
    public class TrainsWagonDTO : TrainsRollingStockDTO
    {
        private int _wagonID;
        private int _wagonType;
        private string _manufacturerName;


        public int WagonID
        {
            get
            {
                return _wagonID;
            }

            set
            {
                _wagonID = value;
            }
        }

        public int WagonType
        {
            get
            {
                return _wagonType;
            }

            set
            {
                _wagonType = value;
            }
        }

        public string ManufacturerName
        {
            get
            {
                return _manufacturerName;
            }

            set
            {
                _manufacturerName = value;
            }
        }
    }
}
