﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WebSite.BusinessLogic.HttpClients.Callers;

namespace WebSite.BusinessLogic.HttpClients.Callers
{
    public class HttpClientCaller : IHttpClientServiceCall
    {
        private const string JsonMediaType = "application/json";

        public HttpClientCaller()
        {
        }

        #region BasicMethods

        public async Task<T1> Get<T1, T2>(string url, string baseAddress, string accessKey, T2 payload) where T2 : class
        {
            return await Send<T1, T2>(HttpMethod.Get, url, baseAddress, accessKey, payload);
        }

        #endregion

        private async Task<T1> Send<T1, T2>(HttpMethod method, string serviceUrl, string baseAddress, string accessKey, T2 payload) where T2 : class
        {
            if ((method == HttpMethod.Get || method == HttpMethod.Delete) && payload != null)
            {
                var parameterString = "";                           /* ToDo Write routine to generate paramter string */
                serviceUrl = $"{serviceUrl}?{parameterString}";
            }

            var message = new HttpRequestMessage
            {
                RequestUri = new Uri(baseAddress+serviceUrl),
                Method = method,
            };

            if ((method == HttpMethod.Post || method == HttpMethod.Put) && payload != null)
            {
                var payloadJson = JsonConvert.SerializeObject(payload);
                message.Content = new StringContent(payloadJson, Encoding.UTF8, JsonMediaType);
            }

            message.Headers.Add("X-Auth-Token", accessKey);
            message.Headers.Accept.Clear();
            message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(JsonMediaType));

            var response = await Client.Get().SendAsync(message);
            var content = await response.Content.ReadAsStringAsync();

            //using (var responseStream = await response.Content.ReadAsStreamAsync())
            //{
            //    using (var streamReader = new StreamReader(responseStream))
            //        using (var jsonTextReader = new JsonTextReader(streamReader))
            //        {
            //            return _jsonSerializer.Deserialize<List<GitHubRepositoryDto>>(jsonTextReader);
            //        }
            //}

            if (!response.IsSuccessStatusCode)
            {
                /* ToDoTaskService */
                throw new System.Exception("Is success Status Code, indicates failure.");
            }

            if (String.IsNullOrWhiteSpace(content))
            {
                return default(T1);
            }

            return JsonConvert.DeserializeObject<T1>(content);
        }

        private static class Client
        {
            private const int TimeOutInMinutes = 5;

            private static DateTime _clientExpiresAt = DateTime.UtcNow.AddMinutes(TimeOutInMinutes);
            private static HttpClient _oldHttpClient = new HttpClient();
            private static HttpClient _newHttpClient = new HttpClient();

            private static object _locker = new object();

            public static HttpClient Get()
            {
                lock (_locker)
                {
                    if (DateTime.UtcNow >= _clientExpiresAt)
                    {
                        _oldHttpClient.Dispose();
                        _oldHttpClient = _newHttpClient;
                        _newHttpClient = new HttpClient();
                        _clientExpiresAt = DateTime.UtcNow.AddMinutes(TimeOutInMinutes);
                    }
                }
                return _newHttpClient;
            }
        }
    }
}

