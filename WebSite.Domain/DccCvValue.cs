﻿

namespace WebSite.Domain
{
    public class DccCvDetails
    {
        private int _CvValueId;

        private int _DecoderId;

        private string _CV;

        private string _Description;

        private string _DataTooltip;

        private bool _Editable;

        public int CvValueId
        {
            get
            {
                return _CvValueId;
            }

            set
            {
                _CvValueId = value;
            }
        }

        public string CV
        {
            get
            {
                return _CV;
            }

            set
            {
                _CV = value;
            }
        }

        public string Description
        {
            get
            {
                return _Description;
            }

            set
            {
                _Description = value;
            }
        }

        public int DecoderId
        {
            get
            {
                return _DecoderId;
            }

            set
            {
                _DecoderId = value;
            }
        }

        public string DataTooltip
        {
            get
            {
                return _DataTooltip;
            }

            set
            {
                _DataTooltip = value;
            }
        }

        public bool Editable
        {
            get
            {
                return _Editable;
            }

            set
            {
                _Editable = value;
            }
        }
    }
}
