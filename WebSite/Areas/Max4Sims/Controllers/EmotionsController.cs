﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Areas.Max4Sims.Controllers
{
    public class EmotionsController : Controller
    {
        // GET: Max4Sims/Emotions
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Emotions()
        {
            return View();
        }

        public ActionResult GoodEmotions() {

            return View();
        }

        public ActionResult NegativeEmotions() {
            return View();
        }

        public ActionResult TeaBrewer()
        {
            return View();
        }

        public ActionResult Mixology()
        {
            return View();
        }

        public ActionResult IceCream()
        {
            return View();
        }

    }
}