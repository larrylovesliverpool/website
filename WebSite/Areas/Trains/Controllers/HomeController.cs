﻿using System.Web.Mvc;
using WebSite.Orchestration;

namespace WebSite.Areas.Trains.Controllers
{
    public class HomeController : TrainsController
    {
        // uses TrainsController, which inherit stand MVC controller
        // trainsController adds extra properties and generic methods

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Bookmarks()
        {
            ViewBag.Area = "Trains";
            return View();
        }

    }
}