﻿using ServicesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Respository;

namespace WebSite.BusinessLogic
{
    public class F1Races
    {
        IF1RacesRepository F1RacesRepo;

        public F1Races()
        {
            F1RacesRepo = new F1Races2018();
        }

        public List<Domain.F1Race> read()
        {
            return GetRaces().ToList();
        }

        public Domain.F1Race NextRace()
        {
            return GetRaces().First(x => x.dateOfRace > DateTime.Now);
        }

        private IList<Domain.F1Race> GetRaces() => F1RacesRepo.Read();
    }
}
