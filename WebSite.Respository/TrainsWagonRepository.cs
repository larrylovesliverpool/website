﻿using System;
using System.Collections.Generic;
using WebSite.Domain;

namespace WebSite.Repository
{
    public class TrainWagonRepository : ServiceInterfaces.ITrainsWagonsRepository
    {
        List<Domain.TrainsWagon> trainWagons = new List<Domain.TrainsWagon>
        {
            new Domain.TrainsWagon() { WagonID =  1, ManufacturerID = 1, Description = "Car Transporter", Quantity = 3 },
            new Domain.TrainsWagon() { WagonID =  2, ManufacturerID = 1, Description = "Tanker (Petrol)", Quantity = 6 },
            new Domain.TrainsWagon() { WagonID =  3, ManufacturerID = 1, Description = "BR Brake Van", Quantity = 2 },
            new Domain.TrainsWagon() { WagonID =  4, ManufacturerID = 1, Description = "Tanker (Milk)", Quantity = 1 },
            new Domain.TrainsWagon() { WagonID =  5, ManufacturerID = 1, Description = "Consort open", Quantity = 3 },
            new Domain.TrainsWagon() { WagonID =  6, ManufacturerID = 1, Description = "NCB open", Quantity = 1 },
            new Domain.TrainsWagon() { WagonID =  7, ManufacturerID = 1, Description = "Container 2x30ft", Quantity = 2, ProductCode = "R6425" },
            new Domain.TrainsWagon() { WagonID =  8, ManufacturerID = 1, Description = "Breakdown Crane BR 75 Ton", Quantity = 1 },
            new Domain.TrainsWagon() { WagonID =  9, ManufacturerID = 1, Description = "Close Van 6 Wheel (Palethorpe Sausages)", Quantity = 2 },
            new Domain.TrainsWagon() { WagonID = 10, ManufacturerID = 1, Description = "BR Ferry Van", Quantity = 3, WagonType = 1 },
        };

        public List<TrainsWagon> Retrieve() => trainWagons;

        public TrainsWagon Retrieve(int key) => trainWagons.Find(i => i.WagonID == key);
        
    }
}
