﻿using System.Web.Http;

namespace WebSite.Controllers
{
    public class TrainsController : ApiController
    {
        BusinessLogic.Trains trains = new BusinessLogic.Trains();

        [HttpGet]
        public IHttpActionResult Get() => Ok(trains.read());
    }
}
