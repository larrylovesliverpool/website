﻿
using WebSite.Domain.DataTransferObjects;

namespace WebSite.ViewModels
{
    public class HomePageVM
    {
        private string _opponent;
        private string _homeAway;
        private string _kickOff;

        private string _barOpponent;
        private string _barHomeAway;
        private string _barkickOff;


        private string _raceName;
        private string _raceVenue;
        private string _raceWeekend;
        private string _raceDay;

        public string Opponent
        {
            get
            {
                return _opponent;
            }

            set
            {
                _opponent = value;
            }
        }

        public string HomeAway
        {
            get
            {
                return _homeAway;
            }

            set
            {
                _homeAway = value;
            }
        }

        public string KickOff
        {
            get
            {
                return _kickOff;
            }

            set
            {
                _kickOff = value;
            }
        }

        public string RaceName
        {
            get
            {
                return _raceName;
            }

            set
            {
                _raceName = value;
            }
        }

        public string RaceVenue
        {
            get
            {
                return _raceVenue;
            }

            set
            {
                _raceVenue = value;
            }
        }

        public string RaceWeekend
        {
            get
            {
                return _raceWeekend;
            }

            set
            {
                _raceWeekend = value;
            }
        }

        public string RaceDay
        {
            get
            {
                return _raceDay;
            }

            set
            {
                _raceDay = value;
            }
        }

        public string BarOpponent
        {
            get
            {
                return _barOpponent;
            }

            set
            {
                _barOpponent = value;
            }
        }

        public string BarHomeAway
        {
            get
            {
                return _barHomeAway;
            }

            set
            {
                _barHomeAway = value;
            }
        }

        public string BarkickOff
        {
            get
            {
                return _barkickOff;
            }

            set
            {
                _barkickOff = value;
            }
        }

        public HomePageVM Generate() => this;

        public HomePageVM Generate(Domain.F1Race races,
                                   FixtureDTO fixture) {

            /* Lfc fixture */
            this.Opponent = fixture.Opposition;
            this.HomeAway = fixture.HomeAway;
            this.KickOff = fixture.Schedule.ToLongDateString() + " at " + fixture.Schedule.ToShortTimeString();

            /* F1 Races */
            this.RaceName = races.race;
            this.RaceVenue = races.location;
            this.RaceWeekend = races.dateOfweekend;

            return this;
        }

        public HomePageVM Generate(Domain.F1Race races,
                                   HomePageFixtureDTO fixtures) {

            /* Lfc fixture */
            if(fixtures.LfcFixture.HasFixture)
            {
                this.Opponent = fixtures.LfcFixture.Opposition;
                this.HomeAway = fixtures.LfcFixture.HomeAway;
                this.KickOff = fixtures.LfcFixture.Schedule.ToLongDateString() + " at " + fixtures.LfcFixture.Schedule.ToShortTimeString();
            }

            /* Barcelona fixtre */
            if (fixtures.BarFixture.HasFixture)
            {
                this.BarOpponent = fixtures.BarFixture.Opposition;
                this.BarHomeAway = fixtures.BarFixture.HomeAway;
                this.BarkickOff = fixtures.BarFixture.Schedule.ToLongDateString();
            }

            /* F1 Races */
            this.RaceName = races.race;
            this.RaceVenue = races.location;
            this.RaceWeekend = races.dateOfweekend;

            return this;
        }

    }
}