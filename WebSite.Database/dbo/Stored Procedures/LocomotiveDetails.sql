﻿CREATE PROCEDURE [dbo].[LocomotiveDetails]
	@LocoID int
AS
	SELECT [LocoId]
      ,[L].[Name] AS LocomotiveName
      ,[Type]
	  ,[T].[Description] AS LocomotiveType
      ,[M].[Name] AS LocomotiveManufacturer
      ,[Decoder]
	  ,[D].[Manufacturer] AS DecoderManufacturer
	  ,[D].[Model] AS DecoderModel
      ,[DCCValue]
      ,[L].[Description]
      ,[YearPurchased]
	FROM [dbo].[Locomotives] L
	LEFT JOIN Manufacturer M ON L.MadeBy = M.ManufacturerId
	LEFT JOIN DCCDecoder D ON L.Decoder = D.DecoderId
	LEFT JOIN LocomotiveType T ON L.Type = T.Id
	WHERE L.LocoId = @LocoID

RETURN 0



