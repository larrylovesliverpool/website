﻿CREATE TABLE [dbo].[BookMarks] (
    [BookMarkID]  INT            NOT NULL,
    [Description] NVARCHAR (50)  NULL,
    [URL]         NVARCHAR (MAX) NULL,
    [Category]    INT            NULL,
    PRIMARY KEY CLUSTERED ([BookMarkID] ASC)
);

