﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain.Railways
{
    public class DecoderCVDescription
    {
        [Key]
        public int Identifier { get; set; }

        public int CVNumber { get; set; }

        public string CVDescription { get; set; }
    }
}
