﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.BusinessLogic.HttpClients.Callers;

namespace WebSite.BusinessLogic.HttpClients
{

    public class HttpClientTableStandings
    {

        private IHttpClientServiceCall _httpClientServiceCall;
        private readonly string _name = "Data Client";

        public IHttpClientServiceCall HttpClientServiceCall
        {
            get { return _httpClientServiceCall; }
            set { _httpClientServiceCall = value; }
        }

        public IHttpClientServiceCall Caller
        {
            get { return _httpClientServiceCall; }
            set { _httpClientServiceCall = value; }
        }

        public string Name
        {
            get { return _name; }
        }

        private static readonly Lazy<HttpClientTableStandings> lazy = new Lazy<HttpClientTableStandings>(() => new HttpClientTableStandings());

        private HttpClientTableStandings()
        {
            HttpClientServiceCall = new HttpClientStandingsCalls();
        }

        public static HttpClientTableStandings Service => lazy.Value;
    }
}
