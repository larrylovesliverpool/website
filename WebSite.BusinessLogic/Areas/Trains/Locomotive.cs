﻿//

using ServicesInterfaces;
using System.Collections.Generic;
using WebSite.Repository;
using WebSite.Domain.DataTransferObjects;
using WebSite.Repository.Dapper;
using WebSite.Domain.Trains;

namespace WebSite.BusinessLogic
{
    public class LocomotiveService
    {
        ILocomotiveRepo locoRepo;

        public LocomotiveService() {
            locoRepo = new LocomotiveRepo();
        }

        public LocomotiveService(ILocomotiveRepo repo)
        {
        }

        public LocomotivesDTO ReadAll()
        {
            LocomotivesDTO dto = new LocomotivesDTO();
            dto.locomotives = new List<LocomotiveDTO>();

            foreach (var loco in locoRepo.GetLocomotives())
            {
                LocomotiveDTO dataItem = new LocomotiveDTO();

                dataItem.Id = loco.LocoId;
                dataItem.Name = loco.Name;
                //dataItem.Manufacturer = loco.Manufacturer;
                dataItem.Description = loco.Description;
                dataItem.DccValue = loco.DCCValue;

                dto.locomotives.Add(dataItem);
            }

            return dto;
        }

        public int Count()
        {
            return locoRepo.GetLocomotives().Count;
        }

        public LocomotiveDetailsDTO ReadDetails(int identifier) {

            var dto = new LocomotiveDetailsDTO();

            var details = locoRepo.GetLocomotivesDetails(identifier);

            if( details != null )
            {
                dto.LocoId = details.LocoId;
                dto.Name = details.Name;
                dto.manufacturer = details.manufacturer;
                dto.PowerType = details.PowerType;
                dto.DCCValue = details.DCCValue;
                dto.Decoder = details.Decoder;
                dto.Classification = details.Classification;
                dto.DecoderId = details.DecoderId;
                dto.Description = details.Description;
                dto.PowerType = details.PowerType;
                dto.ProductCode = details.ProductCode;
                dto.YearPurchased = details.YearPurchased;
            }

            return dto;
        }

        public bool Add(LocomotiveDTO locomotive)
        {
            return false;
        }

        public List<LocomotivesGridViewDomain> ReadGridView()
        {
            var data = locoRepo.GetGridView();

            return data;
        }
    }
}
