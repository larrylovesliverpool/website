﻿
using System.Collections.Generic;
using WebSite.Domain;
using WebSite.Respository;

namespace WebSite.BusinessLogic
{
    public class DccCvDetails
    {
        IDccCvDetailsRepository DccCvValuesRepo;

        public DccCvDetails()
        {
            DccCvValuesRepo = new DccCvDetailsRepository();
        }

        public DccCvDetails(IDccCvDetailsRepository repo)
        {
            DccCvValuesRepo = repo;
        }

        public List<Domain.DccCvDetails> Read() => DccCvValuesRepo.Retrieve();

        public Domain.DccCvDetails Read(int key) => DccCvValuesRepo.Retrieve(key);

        public List<Domain.DccCvDetails> ReadCvValues(int decoderID) => DccCvValuesRepo.RetrieveDecoders(decoderID);
    }
}
