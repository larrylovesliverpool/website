﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Infrastructure
{
    public static class MapperFactory<T>
    {
        public static IViewModelMapper<T> Create()
        {
            var t = AppDomain.CurrentDomain.GetAssemblies()
                            .SelectMany(a => a.GetTypes())
                            .Where(x => x.Namespace == "WebSite.ViewModels")
                            .FirstOrDefault(x => typeof(IViewModelMapper<T>).IsAssignableFrom(x));

            return Activator.CreateInstance(t) as IViewModelMapper<T>;
        }
    }
}