﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class TrainsClassification
    {
        private int _classificationId;

        private string _classification;

        public int ClassificationId
        {
            get
            {
                return _classificationId;
            }

            set
            {
                _classificationId = value;
            }
        }

        public string Classification
        {
            get
            {
                return _classification;
            }

            set
            {
                _classification = value;
            }
        }
    }
}
