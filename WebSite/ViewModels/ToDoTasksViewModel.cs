﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.ViewModels
{
    public class ToDoTask
    {
        public int Identifier { get; set; }

        public string Description { get; set; }

    }

    public class ToDoTasksViewModel
    {
        public ToDoTasksViewModel()
        {
            IsVaild = false;
            ToDoTasks = new List<ToDoTask>();
        }

        public bool IsVaild { get; set; }
        public List<ToDoTask> ToDoTasks { get; set; }
    }
}