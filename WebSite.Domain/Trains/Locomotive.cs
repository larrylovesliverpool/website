using System.ComponentModel.DataAnnotations;

namespace WebSite.Domain.Trains
{
    // ********************************************
    // Domain model maps directly to database table
    // Locomotives
    // ********************************************

    public class Locomotives
    {
        public int LocoId { get; set; }
        public string Name { get; set; }
        public int MadeBy { get; set; }
        public int Decoder { get; set; }
        public int DCCValue { get; set; }
        public string Description { get; set; }
        public int YearPurchased { get; set; }
        public int Classification { get; set; }
        public string ProcductCode { get; set; }
        public int EraModelled { get; set; }
    }
}
