﻿
$(function () {

    getAllFixtures(true);

});

function getAllFixtures(display) {

    // filter json array for liverpool only.
    var search = JSON.search(footballfixturesCalendar, '//*[contains(SUMMARY,"Liverpool")]');

    if (display) {

        // field index name
        var matchdate = 'DTSTART;TZID=Europe/London';
        // current datetime in secs
        var now = Date.now();

        for (var i = 0; i < search.length; i++) {

            // get match date in yyyy-mm-dd format and seperate kick off time
            var matchDay = search[i][matchdate].substring(0, 4) + '-' + search[i][matchdate].substring(4, 6) + '-' + search[i][matchdate].substring(6, 8);
            var matchTime = search[i][matchdate].substring(9, 13);

            var d = new Date(matchDay);
            var n = d.toDateString();
            var secs = Date.parse(matchDay);

            var linesOfText = search[i].DESCRIPTION.split('\\n');
            var teams = linesOfText[0].split(' v ');

            if (linesOfText.length === 4) {
                var onSky = '&nbsp;&nbsp;On Sky';
            }
            else {
                var onSky = '&nbsp;';
            }

            htmlstrg = '<tr id="fixtureRow"' + i + ' >';
            htmlstrg += '<td style="width:25%;text-align:right">';
            htmlstrg += teams[0];
            htmlstrg += '</td>';
            htmlstrg += '<td>&nbsp;v&nbsp;</td>';
            htmlstrg += '<td style="width:25%;text-align:left">';
            htmlstrg += teams[1];
            htmlstrg += '</td>';

            htmlstrg += '<td style="width:20%;text-align:right">' + n + ' ' + matchTime + '</td>';
            htmlstrg += '<td style="width:20%;text-align:left">' + onSky + '</td>';

            htmlstrg += '</tr>';


            /* append html */
            $("#ftb").append(htmlstrg);

        }
    }

    return search;
}