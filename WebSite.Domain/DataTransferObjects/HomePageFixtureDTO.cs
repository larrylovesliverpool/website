﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain.DataTransferObjects
{
    public class HomePageFixtureDTO
    {
        public FixtureDTO LfcFixture { get; set; }

        public FixtureDTO BarFixture { get; set; }
    }
}
