﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSite.BusinessLogic;
using WebSite.Models;
using static WebSite.Areas.Trains.ViewModels.LocomotivesVM;

namespace WebSite.Areas.Trains.helpers
{
    public class LocomotiveHelper
    {
        public LocomotiveHelper()
        {
        }

        public ViewModels.LocomotivesVM GetLocomotivesVM()
        {
            var service = new LocomotiveService();
            var vm = new ViewModels.LocomotivesVM();
            var trainsData = service.ReadGridView();

            foreach (var train in trainsData)
            {
                vm.Locomotives.Add(new LocomotiveTableRow() 
                {                 
                    TableRowID = "RowLoco" + train.LocoId.ToString(),
                    Id = train.LocoId,
                    Name = train.Name,
                    Description = train.Description,
                    DccId = train.DCCId.ToString(),
                    Manufacturer = train.Manufacturer
                });
            }

            return vm;
        }

        public ViewModels.LocomotiveDetailsVM GetLocomotiveDetailsVM(int identifier)
        {
            var service = new LocomotiveService();
            var vm = new ViewModels.LocomotiveDetailsVM();
            var trainDetails = service.ReadDetails(identifier);

            vm.LocoId = trainDetails.LocoId;
            vm.Name = trainDetails.Name;
            vm.manufacturer = trainDetails.manufacturer;
            vm.PowerType = trainDetails.PowerType;
            vm.DCCValue = trainDetails.DCCValue;
            vm.Decoder = trainDetails.Decoder;
            vm.Classification = trainDetails.Classification;
            vm.DecoderId = trainDetails.DecoderId;
            vm.Description = trainDetails.Description;
            vm.PowerType = trainDetails.PowerType;
            vm.ProductCode = trainDetails.ProductCode;
            vm.YearPurchased = trainDetails.YearPurchased;

            return vm;
        }
    }
}