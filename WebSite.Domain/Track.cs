﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class Track
    {
        private string _section;
        private string _description;
        private int _qty;

        public string Section
        {
            get
            {
                return _section;
            }

            set
            {
                _section = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public int Qty
        {
            get
            {
                return _qty;
            }

            set
            {
                _qty = value;
            }
        }
    }
}
