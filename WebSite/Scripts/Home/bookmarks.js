﻿
var PopOutClose = function () {

    $("div[id='PopOut']").each(function () {
        $(this).remove();
    });
};

$(function () {

    $('span[data-item="PopOut"]').on('click', function () {

        var title = $(this).attr('data-title');
        var content = $(this).attr('data-content');

        var htmlStrg = '';

        htmlStrg += '<div id="PopOut" style="right:5px; bottom:-25px; position: absolute; z-index:99" class="well well-sm" >'
        htmlStrg += content;
        htmlStrg += '&nbsp;&nbsp;<span class="glyphicon glyphicon-remove-circle" aria-hidden="true" onclick="PopOutClose()"></span>';
        htmlStrg += '</div>';

        $(this).after(htmlStrg);

    });

    $('span[data-item="PopOut"]').on('mouseenter', function () {

        $(this).css('color', 'blue');
    });

    $('span[data-item="PopOut"]').on('mouseleave', function () {

        $(this).css('color', 'gray');
    });

});