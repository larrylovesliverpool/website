﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class TrainsConfiguration
    {
        private int _ConfigurationId;

        private string _Configuration;

        public int ConfigurationId
        {
            get
            {
                return _ConfigurationId;
            }

            set
            {
                _ConfigurationId = value;
            }
        }

        public string Configuration
        {
            get
            {
                return _Configuration;
            }

            set
            {
                _Configuration = value;
            }
        }
    }
}
