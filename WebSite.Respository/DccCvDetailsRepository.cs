﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.Respository
{
    public class DccCvDetailsRepository : IDccCvDetailsRepository
    {

        List<DccCvDetails> DccCvDetails = new List<DccCvDetails>
        {
            new DccCvDetails() { CvValueId = 2,  DecoderId = 30, CV = "CV1",  Description="Primary Address", DataTooltip="The range of addresses available on a DCC system.", Editable=true },
            new DccCvDetails() { CvValueId = 3,  DecoderId = 30, CV = "CV2",  Description="Vstart", DataTooltip="Controls the minimum voltage applied to the motor on speed step 1.", Editable=true },
            new DccCvDetails() { CvValueId = 4,  DecoderId = 30, CV = "CV3",  Description="Acceleration Rate", DataTooltip="",Editable=true },
            new DccCvDetails() { CvValueId = 5,  DecoderId = 30, CV = "CV4",  Description="Deceleration Rate", DataTooltip="",Editable=true },
            new DccCvDetails() { CvValueId = 6,  DecoderId = 2, CV = "CV5",  Description="VHigh", DataTooltip="Controls the voltage applied to the motor at maximum throttle.", Editable=true },
            new DccCvDetails() { CvValueId = 7,  DecoderId = 2, CV = "CV6",  Description="VMid", DataTooltip="Controls the voltage applied to the motor at the mid point of the throttle setting.", Editable=true },
            new DccCvDetails() { CvValueId = 8,  DecoderId = 2, CV = "CV7",  Description="Manufacturer Version, Manufacturer defined version information", DataTooltip="", Editable=false },
            new DccCvDetails() { CvValueId = 9,  DecoderId = 2, CV = "CV8",  Description="Manufacturer Identifier, Value assigned by NMRA", DataTooltip="",Editable=false },
            new DccCvDetails() { CvValueId = 10, DecoderId = 2, CV = "CV9",  Description="Total PWM Period",DataTooltip="", Editable=true },
            new DccCvDetails() { CvValueId = 11, DecoderId = 2, CV = "CV10", Description="EMF Feedback Cut out", DataTooltip="", Editable=true },
            new DccCvDetails() { CvValueId = 29, DecoderId = 2, CV = "CV29", Description="Decoder Configurations", DataTooltip="", Editable=true }
        };

        public List<DccCvDetails> Retrieve() => DccCvDetails;

        public DccCvDetails Retrieve(int key) => DccCvDetails.Find(i => i.CvValueId == key);

        public List<DccCvDetails> RetrieveDecoders(int decoderID) => DccCvDetails.FindAll(i => (i.DecoderId & decoderID) > 0 );
    }
}
