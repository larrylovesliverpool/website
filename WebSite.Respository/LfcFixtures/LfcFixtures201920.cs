﻿using ServicesInterfaces;
using System.Collections.Generic;
using WebSite.Domain;
using System;

namespace WebSite.Respository
{
    public class LfcFixtures201920 : IFixturesRepository
    {
        private string _teamName = "Liverpool";

        public string TeamName
        {
            get
            {
                return _teamName;
            }
        }

        public fixture read(int identifier)
        {
            throw new NotImplementedException();
        }

        public IList<fixture> ReadAll()
        {
            var fixtures = new List<fixture>
            {
                new fixture() { Schedule = new DateTime(2019,8,11,13,30,00), Opposition = "WestHam United",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,8,20,20,00,00) , Opposition = "Crystal Palace",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,8,25,17,30,00), Opposition = "Bright and Hove Albion",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,9,1,12,30,00), Opposition = "Leicester City",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,9,15,12,30,00), Opposition = "Tottenham Hotspur",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,9,22,15,00,00), Opposition = "Southampton",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,9,29,16,30,00), Opposition = "Chelsea ",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,10,7,12,30,00), Opposition = "Man City",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,10,20,15,00,00), Opposition = "Huddersfield",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,10,27,15,00,00), Opposition = "Cardiff City",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,11,02,15,00,00), Opposition = "Aston Villa",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,11,10,16,30,00), Opposition = "Manchester City",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,11,23,15,00,00), Opposition = "Crystal Palace",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,11,30,15,00,00), Opposition = "Brighton",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,12,04,20,15,00), Opposition = "Everton",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,12,07,12,30,00), Opposition = "Bournemouth",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,12,14,16,30,00), Opposition = "Watford",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,12,19,15,00,00), Opposition = "West Ham",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,12,26,20,00,00), Opposition = "Leicester",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,12,29,16,30,00), Opposition = "Wolves",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2020,01,02,20,00,00), Opposition = "Sheffield United",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2020,01,12,15,00,00), Opposition = "Brighton",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2020,01,19,15,00,00), Opposition = "Crystal Palce",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2020,01,30,20,00,00), Opposition = "Leicester City",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2020,02,01,15,00,00), Opposition = "Southhampton",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2020,02,15,17,30,00), Opposition = "Norwich City",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2020,02,24,20,00,00), Opposition = "West Ham United",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2020,02,29,17,30,00), Opposition = "Watford",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2020,03,02,16,00,00), Opposition = "Everton",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2020,03,09,15,00,00), Opposition = "Burnley",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2020,03,16,15,00,00), Opposition = "Fulham",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2020,03,31,16,30,00), Opposition = "Tottenham Hotspur",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2020,04,06,15,00,00), Opposition = "Southhampton",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2020,04,13,15,00,00), Opposition = "Chelsea",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2020,04,20,15,00,00), Opposition = "Cardiff City",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2020,04,27,15,00,00), Opposition = "Huddersfield Town",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2020,05,04,15,00,00), Opposition = "Newcastle United",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2020,05,12,15,00,00), Opposition = "Wolverhampton Wanderers",HomeAway = "Home" },
            };

            return fixtures;
        }

        public string teamName()
        {
            return TeamName;
        }
    }
}
