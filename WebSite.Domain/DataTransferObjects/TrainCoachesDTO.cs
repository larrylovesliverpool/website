﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain.DataTransferObjects
{
    public class TrainCoachesDTO : BaseModelDTO
    {
        public class ViewModel
        {
            int _coachID { get; set; }
            int _coachType { get; set; }
            string _manufacturerName { get; set; }
            string _productCode { get; set; }
            int _manufacturerID { get; set; }
            string _description { get; set; }
            int _quantity { get; set; }
        }

        private List<ViewModel> _coachesListOf;
        private bool _hasData;

        public List<ViewModel> CoachesListOf
        {
            get
            {
                return CoachesListOf;
            }

            set
            {
                CoachesListOf = value;
            }
        }

        public bool HasData
        {
            get
            {
                return _hasData;
            }

            set
            {
                _hasData = value;
            }
        }
    }
}
