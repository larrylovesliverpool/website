﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSite.Domain.DataTransferObjects;
using WebSite.BusinessLogic;

namespace WebSite.Areas.Trains.Helpers
{
    public static class LocomotiveAddHelper
    {
        public static bool Add(ViewModels.LocomotiveVM vm)
        {
            /* ************** */
            /* move vm to dto */
            /* ************** */

            LocomotiveDTO dto = new LocomotiveDTO();

            dto.Name = vm.Name;
            dto.Description = vm.Description;
            dto.Manufacturer = vm.Manufacturer;
            dto.DccValue = vm.DccValue;
            dto.Decoder = vm.Decoder;

            /* ********************* */
            /* Call to Service layer */
            /* ********************* */

            LocomotiveService service = new LocomotiveService();

            return false;
        }

    }
}