﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.ServiceInterfaces
{
    public interface IRepositoryReader<dataObject>
    {
        List<dataObject> Retrieve();

        dataObject Retrieve(int key);
    }
}
