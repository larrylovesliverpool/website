﻿using System.Web.Mvc;
using WebSite.Areas.Trains.helpers;
using WebSite.Areas.Trains.Helpers;
using WebSite.Domain;

namespace WebSite.Areas.Trains.Controllers
{
    public class LocomotiveController : Controller
    {
        public LocomotiveController() { }

        public ActionResult CreateLocomotive( string LocomotiveName) {

            return View();
        }

        public ActionResult locomotive()
        {
            ViewModels.LocomotivesVM vm = new ViewModels.LocomotivesVM();
            var helper = new LocomotiveHelper();

            return View(helper.GetLocomotivesVM());
        }

        public ActionResult LocomotiveDetails(int identifier)
        {
            return PartialView("_LocomotiveDetails", new LocomotiveHelper().GetLocomotiveDetailsVM(identifier));
        }

        /* *************************** */
        /* Display Add Locomotive View */
        /* *************************** */

        [Authorize]
        public ActionResult Add() => View();

        /* ************************** */
        /* Post locomotive form, Add  */
        /* ************************** */

        [HttpPost]
        public ActionResult Add(ViewModels.LocomotiveVM vm)
        {
            if (ModelState.IsValid) {

                if(LocomotiveAddHelper.Add(vm))
                {
                    RedirectToAction("Locomotive");
                }
            }

            return View();
        }
    }
}