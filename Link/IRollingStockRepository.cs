﻿
//

namespace ServicesInterfaces
{
    public interface IRollingStockRepository
    {
        void Create();

        void Read();

        void Update();

        void Delete();
    }
}
