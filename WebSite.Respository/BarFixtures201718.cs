﻿using ServicesInterfaces;
using System.Collections.Generic;
using WebSite.Domain;
using System;

namespace WebSite.Respository
{
    public class BarFixtures201718 : IFixturesRepository
    {
        private string _teamName = "Barcelona";

        public string TeamName
        {
            get
            {
                return _teamName;
            }
        }

        public fixture read(int identifier)
        {
            throw new NotImplementedException();
        }

        public IList<fixture> ReadAll()
        {
            var fixtures = new List<fixture>
            {

                new fixture() { Schedule = new System.DateTime(2017,8,20), Opposition = "Real Betis",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2017,8,27) , Opposition = "Alaves",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2017,9,10), Opposition = "Espanyol",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2017,9,17), Opposition = "Getafe CF",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,2,8), Opposition = "Valencia",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,2,11), Opposition = "Getafe",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,2,17), Opposition = "Eibar",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,2,20), Opposition = "Chelsea",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,2,24), Opposition = "Girona",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,3,1), Opposition = "Las Palmas",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,3,4), Opposition = "Atletico Madrid",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,3,11), Opposition = "Malaga",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,3,14), Opposition = "Chelsea",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,3,18), Opposition = "Ath. Bilbao",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,4,1), Opposition = "Sevilla",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,4,8), Opposition = "Leganes",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,4,15), Opposition = "Valencia",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,4,18), Opposition = "Celta Vigo",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,4,21), Opposition = "Sevilla",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,4,22), Opposition = "Villarreal",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,4,29), Opposition = "Deportivo",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,5,6), Opposition = "Real Madrid",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,5,13), Opposition = "Levante",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,5,18), Opposition = "Real Sociedad",HomeAway = "Home" },
            };



            return fixtures;
        }

        public string teamName()
        {
            return TeamName;
        }
    }
}
