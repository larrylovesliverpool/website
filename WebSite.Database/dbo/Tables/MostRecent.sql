﻿CREATE TABLE [dbo].[MostRecent] (
    [Id]          INT           NOT NULL,
    [DateTime]    DATETIME      NULL,
    [Description] VARCHAR (50)  NULL,
    [url]         VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

