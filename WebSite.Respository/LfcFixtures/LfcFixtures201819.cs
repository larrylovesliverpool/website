﻿using ServicesInterfaces;
using System.Collections.Generic;
using WebSite.Domain;
using System;

namespace WebSite.Respository
{
    public class LfcFixtures201819 : IFixturesRepository
    {
        private string _teamName = "Liverpool";

        public string TeamName
        {
            get
            {
                return _teamName;
            }
        }

        public fixture read(int identifier)
        {
            throw new NotImplementedException();
        }

        public IList<fixture> ReadAll()
        {
            var fixtures = new List<fixture>
            {
                new fixture() { Schedule = new DateTime(2018,8,11,13,30,00), Opposition = "WestHam United",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,8,20,20,00,00) , Opposition = "Crystal Palace",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,8,25,17,30,00), Opposition = "Bright and Hove Albion",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,9,1,12,30,00), Opposition = "Leicester City",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,9,15,12,30,00), Opposition = "Tottenham Hotspur",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,9,22,15,00,00), Opposition = "Southampton",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,9,29,16,30,00), Opposition = "Chelsea ",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,10,7,12,30,00), Opposition = "Man City",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,10,20,15,00,00), Opposition = "Huddersfield",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,10,27,15,00,00), Opposition = "Cardiff City",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,11,03,15,00,00), Opposition = "Arsenal",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,11,10,15,00,00), Opposition = "Fulham",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,11,24,15,00,00), Opposition = "Watford",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,12,02,16,00,00), Opposition = "Everton",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,12,05,17,45,00), Opposition = "Burnley FC",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,12,08,12,30,00), Opposition = "Bournemouth",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,12,16,16,00,00), Opposition = "Man United",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,12,21,20,00,00), Opposition = "Wolerhampton Wanderers",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,12,26,15,00,00), Opposition = "Newcastle",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,12,29,15,00,00), Opposition = "Arsenal",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,01,01,15,00,00), Opposition = "Man City",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,01,12,15,00,00), Opposition = "Brighton",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,01,19,15,00,00), Opposition = "Crystal Palce",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,01,30,20,00,00), Opposition = "Leicester City",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,02,04,20,00,00), Opposition = "West Ham United",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,02,09,15,00,00), Opposition = "Bournemouth",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,02,24,14,05,00), Opposition = "Manchester United",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,02,27,20,00,00), Opposition = "Watford",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,03,02,16,00,00), Opposition = "Everton",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,03,09,15,00,00), Opposition = "Burnley",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,03,16,15,00,00), Opposition = "Fulham",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,03,31,16,30,00), Opposition = "Tottenham Hotspur",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,04,06,15,00,00), Opposition = "Southhampton",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,04,13,15,00,00), Opposition = "Chelsea",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,04,20,15,00,00), Opposition = "Cardiff City",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,04,27,15,00,00), Opposition = "Huddersfield Town",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2019,05,04,15,00,00), Opposition = "Newcastle United",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2019,05,12,15,00,00), Opposition = "Wolverhampton Wanderers",HomeAway = "Home" },
            };

            return fixtures;
        }

        public string teamName()
        {
            return TeamName;
        }
    }
}
