﻿using System.Collections.Generic;
using WebSite.Domain;

namespace ServicesInterfaces
{
    public interface IF1RacesRepository
    {
        List<F1Race> Read();
    }
}
