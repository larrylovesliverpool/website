﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Domain.Trains;

namespace WebSite.Domain.DataTransferObjects
{
    public class LocomotiveDTO
    {
        private int _id;
        private string _name;
        private string _description;
        private int _manufacturer;
        private string _decoder;
        private int _dccValue;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public int Manufacturer
        {
            get
            {
                return _manufacturer;
            }

            set
            {
                _manufacturer = value;
            }
        }

        public string Decoder
        {
            get
            {
                return _decoder;
            }

            set
            {
                _decoder = value;
            }
        }

        public int DccValue
        {
            get
            {
                return _dccValue;
            }

            set
            {
                _dccValue = value;
            }
        }

        public int Id { get => _id; set => _id = value; }
    }

    public class LocomotivesDTO
    {
        public List<LocomotiveDTO> locomotives { get; set; }
    }
}
