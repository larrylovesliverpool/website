﻿using System;
using System.Collections.Generic;
using WebSite.Domain;

namespace WebSite.Respository
{
    public class TrainsManufacturerRepository : ServiceInterfaces.ITrainsManufacturerRepository
    {
        List<Domain.TrainsManufacturer> trainManufacturers = new List<Domain.TrainsManufacturer>
        {
            new Domain.TrainsManufacturer() { manufacturerID = 1, name = "Hornby" },
            new Domain.TrainsManufacturer() { manufacturerID = 2, name = "Bachmann" },
            new Domain.TrainsManufacturer() { manufacturerID = 3, name = "Dapol" },
            new Domain.TrainsManufacturer() { manufacturerID = 4, name = "Heljan" }
        };

        public List<TrainsManufacturer> Retrieve() => trainManufacturers;

        public TrainsManufacturer Retrieve(int key) => trainManufacturers.Find(i => i.manufacturerID == key);
    }
}
