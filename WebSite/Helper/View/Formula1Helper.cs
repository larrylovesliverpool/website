﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSite.BusinessLogic.HttpClients.Services;

namespace WebSite.Helper.View
{
    public static class Formula1Helper
    {
        public static Domain.Formula1.Formula1Races GetRaces() {

            Formula1Service f1Service = new Formula1Service();
            var data = f1Service.GetRaces();

            return data;
        }

        public static void GetRace(int round) { }

        public static void GetNextRace() { }
    }
}