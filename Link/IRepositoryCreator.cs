﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.ServiceInterfaces
{
    public interface IRepositoryCreator<dataObject>
    {
        int Insert(List<dataObject> T);

        int Insert(dataObject T);
    }
}
