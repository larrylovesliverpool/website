﻿using System;
using System.Collections.Generic;
using WebSite.Domain;
using WebSite.Domain.DataTransferObjects;
using WebSite.ViewModels.BaseModels;

namespace WebSite.ViewModels
{
    public class WagonsVM : ErrorCondition, IViewDtoModel<WagonsVM, TrainsWagonDTO>
    {
        /* ********************************
         * View model, include error status
         * ******************************** */

        public class ViewModel : TrainsWagon
        {
            public string manufacturerName { get; set; }
        }

        public bool hasRows { get; set; }
        public List<ViewModel> wagonsListOf { get; set; }

        public List<Domain.TrainsWagon> wagons = new List<Domain.TrainsWagon>();

        public WagonsVM generateViewModel()
        {
            BusinessLogic.TrainsWagons wagonDetails = new BusinessLogic.TrainsWagons();
            wagonDetails.read().ForEach(i => ToMapper(i));

            return this;
        }

        private void ToMapper(Domain.TrainsWagon wagon)
        {
            wagons.Add(new Domain.TrainsWagon() {
                WagonID = wagon.WagonID,
                Description = wagon.Description,
                Quantity = wagon.Quantity,
                ProductCode = wagon.ProductCode });
        }

        private void ToVM(TrainsWagonDTO wagon)
        {
            wagonsListOf.Add(new ViewModel() {

                WagonID = wagon.WagonID,
                WagonType = wagon.WagonType,
                Description = wagon.Description,
                Quantity = wagon.Quantity,
                ProductCode = wagon.ProductCode
            });
        }

        public WagonsVM GenerateViewModel()
        {
            BusinessLogic.TrainsWagons wagonDetails = new BusinessLogic.TrainsWagons();

            var wagonsListOf = wagonDetails.ReadWithDetails();

            this.hasRows = wagonsListOf.hasData;

            //wagonDetails.ReadWithDetails().ForEach(wagon => ToVM(wagon));

            return this;
        }

    }
}