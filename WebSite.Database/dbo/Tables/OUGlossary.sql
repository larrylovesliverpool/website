﻿CREATE TABLE [dbo].[OUGlossary]
(
	[Identifier] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NULL, 
    [Description] NVARCHAR(MAX) NULL
)
