﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.Helper.View;

namespace WebSite.Controllers
{
    public class Formula1Controller : Controller
    {
        CultureInfo culture = new CultureInfo("en-GB");

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Races()
        {
            /* ViewModel */
            var vm = new ViewModels.Formula1.RacesViewModel();
            vm.ListOf = new List<ViewModels.Formula1.Race>();

            /* BLL */
            var races = Formula1Helper.GetRaces();

            /* Build ViewModel */
            foreach (var race in races.MRData.RaceTable.Races)
            {
                vm.ListOf.Add(new ViewModels.Formula1.Race
                {
                    round = race.round,
                    raceName = race.raceName,
                    Circuit = race.Circuit.circuitName,
                    Country = race.Circuit.Location.country,
                    date = Convert.ToDateTime(race.date,culture).ToString("dd-MM-yyyy"),
                    time = race.time
                });
            }

            return View(vm);
        }
    }
}