﻿using ServicesInterfaces;
using System.Collections.Generic;
using WebSite.Domain;
using System;

namespace WebSite.Respository
{
    public class LfcFixtures201718 : IFixturesRepository
    {
        private string _teamName = "Liverpool";

        public string TeamName
        {
            get
            {
                return _teamName;
            }
        }

        public fixture read(int identifier)
        {
            throw new NotImplementedException();
        }

        public IList<fixture> ReadAll()
        {
            var fixtures = new List<fixture>
            {
                new fixture() { Schedule = new DateTime(2018,8,11,12,30,00), Opposition = "WestHam United",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,8,18,15,00,00) , Opposition = "Crystal Palace",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,8,25,16,00,00), Opposition = "Bright and Hove Albion",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,9,1,12,30,00), Opposition = "Leicester City",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,9,16,15,00,00), Opposition = "Burnley",HomeAway = "Home" },
                new fixture() { Schedule = new DateTime(2018,9,23,17,30,00), Opposition = "Leicester City",HomeAway = "Away" },
                new fixture() { Schedule = new DateTime(2018,10,1,16,30,00), Opposition = "Newcastle",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,10,14,12,30,00), Opposition = "Man United",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,10,22,16,00,00), Opposition = "Tottenham",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,10,28,15,00,00), Opposition = "Huddersfield",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,11,04,17,30,00), Opposition = "West Ham",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,11,18,15,00,00), Opposition = "Southhampton",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,11,25,17,30,00), Opposition = "Chelsea",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,11,29,15,00,00), Opposition = "Stoke City",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,12,02,15,00,00), Opposition = "Brighton",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,12,09,15,00,00), Opposition = "Everton",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,12,13,15,00,00), Opposition = "West Brom",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,12,16,15,00,00), Opposition = "Bournemouth",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2017,12,23,15,00,00), Opposition = "Arsenal",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2017,12,16,15,00,00), Opposition = "Swansea",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2017,12,30,15,00,00), Opposition = "Leicester City",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,01,01,15,00,00), Opposition = "Burnley",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,01,13,15,00,00), Opposition = "Man City",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,01,20,15,00,00), Opposition = "Swansea",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,01,30,19,45,00), Opposition = "Huddersfield",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,02,03,15,00,00), Opposition = "Tottenham",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,02,10,15,00,00), Opposition = "Southhampton",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,02,24,15,00,00), Opposition = "West Ham United",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,03,03,15,00,00), Opposition = "Newcastle",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,03,10,15,00,00), Opposition = "Man United",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,03,17,15,00,00), Opposition = "Watford",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,03,31,15,00,00), Opposition = "Crystal Palace",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,04,07,15,00,00), Opposition = "Everton",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,04,14,15,00,00), Opposition = "Bournemouth",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,04,21,15,00,00), Opposition = "West Brom",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,04,28,15,00,00), Opposition = "Stoke City",HomeAway = "Home" },
                new fixture() { Schedule = new System.DateTime(2018,05,05,15,00,00), Opposition = "Chelsea",HomeAway = "Away" },
                new fixture() { Schedule = new System.DateTime(2018,05,13,15,00,00), Opposition = "Brighton",HomeAway = "Home" },
            };

            return fixtures;
        }

        public string teamName()
        {
            return TeamName;
        }
    }
}
