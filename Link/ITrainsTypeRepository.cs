﻿using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.Respository
{
    public interface ITrainsTypeRepository : IRepositoryReader<TrainsType> { }
}