﻿
using System.Collections.Generic;
using WebSite.Domain.BaseModels;

namespace WebSite.Domain.dto
{
    public class BookMarksModel
    {
        private List<BookMark> _BookMarks;
        private ErrorMessage _ErrorCondition;

        public List<BookMark> BookMarks
        {
            get
            {
                return _BookMarks;
            }

            set
            {
                _BookMarks = value;
            }
        }

        public ErrorMessage ErrorCondition
        {
            get
            {
                return _ErrorCondition;
            }

            set
            {
                _ErrorCondition = value;
            }
        }

        public BookMarksModel() {
            ErrorCondition = new ErrorMessage();
        }
        
    }
}
