﻿CREATE PROCEDURE [dbo].[Locomotive_SelectAll]

AS
	SELECT	L.Name, 
			T.Description,
			M.Name, 
			D.Manufacturer + ' ' + D.Model AS Decoder,
			L.DCCValue
	FROM Locomotives L
	INNER JOIN LocomotiveType T
	ON L.Type = T.Id
	INNER JOIN Manufacturer M
	ON L.MadeBy = M.ManufacturerId
	INNER JOIN [larry].DCCDecoder D
	ON L.Decoder = D.DecoderId
RETURN 0