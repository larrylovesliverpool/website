﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class F1Race
    {
        public string race { get; set; }
        public string location { get; set; }
        public DateTime dateOfRace { get; set; }
        public string dateOfweekend { get; set; }
    }
}
