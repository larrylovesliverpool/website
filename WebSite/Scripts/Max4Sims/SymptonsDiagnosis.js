﻿
// ailments object

var ailments = {
    'starry_eyes': {
        'symptoms': ['dizziness', 'swirl_rash', 'swatting'],
        'name': 'Starry Eyes'
    },
    'bloaty_head': {
        'symptoms': ['head_steam', 'headache', 'spot_rash'],
        'name': 'Bloaty Head'
    },
    'gas_and_giggles': {
        'symptoms': ['stomach_ache', 'food_bubble', 'thermometer_bubble', 'giggling', 'stripes_rash', 'passing_gas_bubble', 'passing_gas'],
        'name': 'Gas-and-Giggles'
    },
    'llama_flu': {
        'symptoms': ['spot_rash', 'sneeze', 'cough', 'thermometer_bubble', 'fever', 'pepto_bubble'],
        'name': 'Llama Flu'
    },
    'sweaty_shivers': {
        'symptoms': ['spot_rash', 'itchiness', 'thermometer_bubble', 'fever'],
        'name': 'Sweaty Shivers'
    },
    'itchy_plumbob': {
        'symptoms': ['itchiness', 'giggling', 'stripes_rash', 'spot_rash'],
        'name': 'Itchy Plumbob'
    },
    'burnin_belly': {
        'symptoms': ['stomach_ache', 'pepto_bubble', 'fever'],
        'name': 'Burnin\' Belly'
    },
    'triple_threat': {
        'symptoms': ['dizziness', 'sneeze', 'itchiness', 'cough', 'stripes_rash', 'spot_rash', 'swirl_rash', 'pepto_bubble', 'passing_gas'],
        'name': 'Triple Threat'
    }
};

var clearCheckedSymptons = function () {
    $("input[name='symptoms']").removeAttr('checked');
    $("#diagnosis_list div").html("No diagnosis available");
};

var allIn = function (subset, master) {
    var allAreIn = true;
    for (var i = 0; i < subset.length; i++) {
        var itemIndex = master.indexOf(subset[i]);
        if (itemIndex === -1) {
            return false;
        }
    }
    return true;
}


$(function () {

    $('input[name="symptoms"]').on('click', function () {

        var symptoms = [];
        var conditions = [];

        $.each($("input[name='symptoms']:checked"), function () {
            symptoms.push($(this).val());
        });

        if (symptoms.length > 0) {
            for (var ailment in ailments) {
                if (allIn(symptoms, ailments[ailment].symptoms)) {
                    conditions.push(ailments[ailment].name)
                }
            }
        }

        if (conditions.length > 0) {
            $("#diagnosis_list div").html(conditions.join("<br>"));
        } else {
            $("#diagnosis_list div").html("No diagnosis available");
        }
    });
});
