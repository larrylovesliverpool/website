﻿using System.Collections.Generic;
using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.ServiceInterfaces
{
    public interface ITrainsCoaches : IRepositoryReader<TrainsCoach> {

        List<TrainsCoach> Retrieve(string searchText);
        int total();
    }
}
