﻿CREATE TABLE [dbo].[Manufacturer] (
    [ManufacturerId] INT           NOT NULL,
    [Name]           NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([ManufacturerId] ASC)
);

