﻿using System;
using WebSite.Infrastructure;

namespace WebSite.ViewModels
{
    public class VM_LatestBookMark
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public DateTime LastAccess { get; set; }
        public imgColors Color { get; set; }

        public VM_LatestBookMark() {

            Color = imgColors.None;
        }
    }
}