﻿using AutoMapper;
using System.Collections.Generic;
using WebSite.Domain;

namespace WebSite.ViewModels
{
    public class DccDecoderVM : BaseModels.ErrorCondition
    {
        public List<DccCvDetails> cvDetails { get; set; }
        public DccDecoder decoderDetails { get; set; }
        public string title { get; set; }

        public DccDecoderVM Generate() {
            return this;
        }

        public DccDecoderVM Generate(int identifier) {

            cvDetails = new BusinessLogic.DccCvDetails().ReadCvValues(identifier);
            decoderDetails = new Respository.DccDecodersRepository().Retrieve(identifier);
            title = decoderDetails.manufacturer + " " + decoderDetails.description;

            return this;
        }

        public DccDecoderVM Generate(DccDecoderVM viewModel)
        {
            viewModel.title = viewModel.decoderDetails.manufacturer + viewModel.decoderDetails.description;

            return viewModel;
        }

        public static implicit operator DccDecoderVM(Domain.DccDecoder decoder)
        {
            return Mapper.Map<DccDecoderVM>(decoder);
        }

    }

}