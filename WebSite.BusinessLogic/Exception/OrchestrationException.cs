﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.BusinessLogic.Exception
{
    public class OrchestrationException : SystemException
    {
        public int ErrorCode { get; set; }
        public bool Email { get; set; }

        public OrchestrationException() : base() { }
        public OrchestrationException(string message) : base(message) { }
        public OrchestrationException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected OrchestrationException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
        { }
    }
}
