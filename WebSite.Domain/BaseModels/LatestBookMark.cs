﻿using System;

namespace WebSite.Domain.BaseModels
{
    public class LatestBookMark
    {
        private string _title;
        private string _url;
        private DateTime _lastAccess;

        public string Title
        {
            get
            {
                return _title;
            }

            set
            {
                _title = value;
            }
        }

        public string Url
        {
            get
            {
                return _url;
            }

            set
            {
                _url = value;
            }
        }

        public DateTime LastAccess
        {
            get
            {
                return _lastAccess;
            }

            set
            {
                _lastAccess = value;
            }
        }
    }
}
