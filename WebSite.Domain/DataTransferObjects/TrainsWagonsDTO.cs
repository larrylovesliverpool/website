﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class TrainsWagonsDTO : BaseModelDTO
    {
        public class ViewModel
        {
            int _wagonID { get; set; }
            int _wagonType { get; set; }
            string _manufacturerName { get; set; }
            string _productCode { get; set; }
            int _manufacturerID { get; set; }
            string _description { get; set; }
            int _quantity { get; set; }
        }

        private List<ViewModel> _wagonsListOf;
        private bool _hasData;

        public List<ViewModel> WagonsListOf
        {
            get
            {
                return WagonsListOf;
            }

            set
            {
                WagonsListOf = value;
            }
        }

        public bool HasData
        {
            get
            {
                return _hasData;
            }

            set
            {
                _hasData = value;
            }
        }
    }
}
