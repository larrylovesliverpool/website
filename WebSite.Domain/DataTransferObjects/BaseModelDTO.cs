﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public abstract class BaseModelDTO
    {
        private bool _hasData;

        public bool hasData
        {
            get
            {
                return _hasData;
            }

            set
            {
                _hasData = value;
            }
        }
    }
}
