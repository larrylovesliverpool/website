﻿
CREATE TABLE [dbo].[Locomotives](
	[LocoId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Type] [int] NULL,
	[MadeBy] [int] NULL,
	[Decoder] [int] NULL,
	[DCCValue] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[YearPurchased] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[LocoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



