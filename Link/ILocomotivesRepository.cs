﻿
//

using System.Collections.Generic;
using WebSite.Domain;

namespace ServicesInterfaces
{
    public interface ILocomotivesRepository
    {
        IList<Locomotive> read();

        Locomotive read(int identifier);
    }
}
