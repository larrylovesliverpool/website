﻿
using System.Web.Mvc;

using WebSite.Areas.Max4Sims.ViewModels;
using WebSite.Domain.DataTransferObjects;

namespace WebSite.Areas.Max4Sims.Controllers
{
    public class CollectionsController : Controller
    {
        public ActionResult Index() { return View(); }

        public ActionResult Fish() { return View(); }

        public ActionResult Plants() { return View(); }

        public ActionResult Frogs() { return View(); }

        public ActionResult MicroscopePrints() { return View(); }

        public ActionResult Postcards() { return View(); }

        public ActionResult SpacePrints() { return View(); }

        public ActionResult SnowGlobes() {

            SnowGlobesVM snowGlobesVM = new SnowGlobesVM();
            snowGlobesVM.SnowGlobes = new System.Collections.Generic.List<SnowGlobeVM>();

            var SnowGlobesDTO = new BusinessLogic.SnowGlobes().GetAll();

            foreach ( SnowGlobeDTO dto in SnowGlobesDTO.ListOf)
            {
                snowGlobesVM.SnowGlobes.Add(new SnowGlobeVM() { Name = dto.Name, Comments = dto.Comments, Rarity = dto.Rarity });
            }

            return View(snowGlobesVM);
        }

        public ActionResult Fossils()
        {
            FossilsVM fossilsVM = new FossilsVM();
            fossilsVM.Fossils = new System.Collections.Generic.List<FossilVM>();

            var fossilsDTO = new BusinessLogic.Fossil().GetAll();

            foreach (FossilDTO dto in fossilsDTO.ListOf)
            {
                fossilsVM.Fossils.Add(new FossilVM() { Name = dto.Name, Comments = dto.Comments, Rarity = dto.Rarity });
            }

            return View(fossilsVM);
        }
    }
}