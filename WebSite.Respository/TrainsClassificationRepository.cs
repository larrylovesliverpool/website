﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Domain;

namespace WebSite.Respository
{
    public class TrainsClassificationRepository
    {
        List<TrainsClassification> configurations = new List<TrainsClassification>()
        {
            new TrainsClassification() { ClassificationId = 1, Classification = "Express Passenger" },
            new TrainsClassification() { ClassificationId = 2, Classification = "Freight" },
            new TrainsClassification() { ClassificationId = 3, Classification = "Mixed Traffic" },
            new TrainsClassification() { ClassificationId = 4, Classification = "Passenger" },
        };

        public List<TrainsClassification> Retrieve() => configurations;

        public TrainsClassification Retrieve(int key) => configurations.Find(p => p.ClassificationId == key);
    }
}
