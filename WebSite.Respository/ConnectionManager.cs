﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebSite.Repository
{
    public class ConnectionManager : IConnectionManager
    {
        private string _ConnectionName;
        private SqlConnection _Connection = null;

        public string ConnectionName
        {
            get
            {
                return _ConnectionName;
            }

            set
            {
                _ConnectionName = value;
            }
        }

        public SqlConnection Connection
        {
            get
            {
                return _Connection;
            }

            set
            {
                _Connection = value;
            }
        }

        public ConnectionManager(string name)
        {
            ConnectionName = name;

            Connection = sqlConnection(connectionString(name));
        }

        public void Close()
        {
            try
            {
                if (Connection.State == ConnectionState.Open) Connection.Close();
            }
            catch
            {

            }
        }

        public void Open()
        {
            try
            {
                if (Connection.State == ConnectionState.Closed) Connection.Open();
            }
            catch
            {

            }
        }

        public string connectionString()
        {
            return ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString;
        }

        public string connectionString(string connection)
        {
            return ConfigurationManager.ConnectionStrings[connection].ConnectionString;
        }

        public SqlConnection sqlConnection()
        {
            return new SqlConnection();
        }

        public SqlConnection sqlConnection(string connectionString) {
            return new SqlConnection(connectionString);
        }
    }
}
