﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Areas.Trains.Controllers
{
    public class TrackController : Controller
    {
        // GET: Trains/Track
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Track() => View();
    }
}