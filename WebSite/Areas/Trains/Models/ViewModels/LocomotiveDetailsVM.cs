﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Trains.ViewModels
{
    public class LocomotiveDetailsVM
    {
        public int LocoId { get; set; }
        public string Name { get; set; }
        public string manufacturer { get; set; }
        public string Decoder { get; set; }
        public int DecoderId { get; set; }
        public int DCCValue { get; set; }
        public string Description { get; set; }
        public int YearPurchased { get; set; }
        public string Classification { get; set; }
        public string PowerType { get; set; }
        public string ProductCode { get; set; }
    }
}