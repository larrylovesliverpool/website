﻿using System;
using System.Collections.Generic;
using WebSite.BusinessLogic;
using WebSite.Domain;
using WebSite.ViewModels.BaseModels;

namespace WebSite.Areas.Trains.ViewModels
{
    public class LocomotivesVM : ErrorCondition
    {
        public class LocomotiveTableRow
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Manufacturer { get; set; }
            public string DccId { get; set; }
            public string YearPurchased { get; set; }
            public string TableRowID { get; set; }
        }

        public List<LocomotiveTableRow> Locomotives { get; set; }

        public LocomotivesVM() {

            Locomotives = new List<LocomotiveTableRow>();
        }

    }
}