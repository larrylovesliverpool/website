﻿using System.Collections.Generic;
using System.Linq;
using WebSite.Domain;
using WebSite.Domain.DataTransferObjects;

namespace WebSite.BusinessLogic
{
    public class TrainsWagons
    {
        ServiceInterfaces.ITrainsWagonsRepository wagonsRepo;
        ServiceInterfaces.ITrainsManufacturerRepository manufRepo;

        public TrainsWagons()
        {
            wagonsRepo = new WebSite.Repository.TrainWagonRepository();
        }

        public TrainsWagons(ServiceInterfaces.ITrainsWagonsRepository repo)
        {
            wagonsRepo = repo;
        }

        public List<TrainsWagon> read() => wagonsRepo.Retrieve();

        public TrainsWagon read(int key) => wagonsRepo.Retrieve(key);

        public TrainsWagonsDTO ReadWithDetails()
        {
            TrainsWagonsDTO wagonsListOf = new TrainsWagonsDTO();

            try
            {
                var wagons = wagonsRepo.Retrieve();
                var manufs = manufRepo.Retrieve();

                var fulldetails = from wagon in wagons
                                  join manuf in manufs on wagon.ManufacturerID equals manuf.manufacturerID
                                  select new
                                  {
                                      WagonID = wagon.WagonID,
                                      WagonType = wagon.WagonType,
                                      manufacturerName = manuf.name,
                                  };


                wagonsListOf.hasData = true;
            }
            catch
            {
                wagonsListOf.hasData = false;
            }

            return wagonsListOf;
        }

        public TrainsWagonDTO ReadWithDetails(int key)
        {
            TrainsWagonDTO wagonDetails = new TrainsWagonDTO();

            try
            {
                var wagons = wagonsRepo.Retrieve();
                var manufs = manufRepo.Retrieve();

                var fulldetails = from wagon in wagons
                                  join manuf in manufs on wagon.ManufacturerID equals manuf.manufacturerID
                                  where wagon.WagonID == key
                                  select new
                                  {
                                      WagonID = wagon.WagonID,
                                      WagonType = wagon.WagonType,
                                      manufacturerName = manuf.name,
                                  };

                var singleDetail = fulldetails.First();

                wagonDetails.WagonID = singleDetail.WagonID;
                wagonDetails.WagonType = singleDetail.WagonType;
                wagonDetails.ManufacturerName = singleDetail.manufacturerName;


                wagonDetails.hasData = true;
            }
            catch
            {
                wagonDetails.hasData = false;
            }

            return wagonDetails;
        }

        private TrainsWagonDTO ToDTO()
        {
            TrainsWagonDTO dto = new TrainsWagonDTO();

            return dto;
        }

    }
}
