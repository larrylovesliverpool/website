﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.ViewModels
{

    public interface IViewDtoModel<viewObject, dtoObject>
    {
        viewObject GenerateViewModel();

        //viewObject GenerateViewModel(dtoObject dto);
    }

    public interface IViewModel<viewObject>
    {
        viewObject generateViewModel();
    }
}


