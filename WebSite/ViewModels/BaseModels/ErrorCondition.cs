﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.ViewModels.BaseModels
{
    public class ErrorCondition : BaseModelVM
    {
        private bool _hasError;
        private string _errorMessage;

        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }

            set
            {
                _errorMessage = value;
            }
        }

        public bool HasError
        {
            get
            {
                return _hasError;
            }

            set
            {
                _hasError = value;
            }
        }
    }
}