﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using WebSite.ViewModels;

namespace WebSite.Areas.Development.Controllers
{
    public class MyClass
    {
        public List<User> data { get; set; }
    }

    public class User
    {
        public string name { get; set; }
        public string id { get; set; }
    }

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var view = new ViewResult();

            return view;
        }

        public ActionResult PartialViewAJAX()
        {
            return View();
        }

        public ActionResult ActionPartial()
        {
            var vm = new PartialViewVM();

            vm.name = "flying Scottsman";
            vm.desc = "Steam train";

            var data = @"
    {
        ""data"": [
            {
                ""name"": ""A Jones"",
                ""id"": ""500015763""
            },
            {
                ""name"": ""B Smith"",
                ""id"": ""504986213""
            },
            {
                ""name"": ""C Brown"",
                ""id"": ""509034361""
            }
        ]
    }
";


            var des = (MyClass)Newtonsoft.Json.JsonConvert.DeserializeObject(data, typeof(MyClass));


            return PartialView("_Partial",vm);
        }

        public ActionResult Solid()
        {
            return View();
        }

        public ActionResult OOP()
        {
            return View();
        }

        public ActionResult Dependency()
        {
            return View();
        }

        public ActionResult CodeSnippets()
        {
            return View();
        }

        public ActionResult Bookmarks()
        {
            ViewBag.Area = "Computer";
            return View();
        }

        public ActionResult AutoMapper()
        {
            return View();
        }

        public ActionResult Mapper() {

            return View();
        }

        [HttpGet]
        public ActionResult Locomotive()
        {
            return Json( new Domain.Locomotive() { Description = "json data model"}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Locomotive(int identifier)
        {
            return Json(new Domain.Locomotive() { Description = "json data model, using a parameter" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult upLoad()
        {
            System.Web.HttpPostedFileBase file;

            if (Request.Files.Count > 0)
            {
                file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                    file.SaveAs(path);
                }
            }

            return View();
        }

    }
}