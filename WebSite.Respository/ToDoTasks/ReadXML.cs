﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WebSite.Domain;
using System.IO;
using System.Web;

namespace WebSite.Respository
{
    public class ToDoTasksRepository
    {
        public IList<ToDoTask> ReadTasks()
        {
            IList<ToDoTask> tasks = new List<ToDoTask>();

            //Load the XML file in XmlDocument.
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(HttpContext.Current.Server.MapPath("~/App_Data/XMLFileToDoTasks.xml"));

            //Loop through the selected Nodes.
            foreach (XmlNode node in xmlDoc.SelectNodes("/Tasks/Task"))
            {
                //Fetch the Node values and assign it to Model.
                tasks.Add(new ToDoTask()
                {
                    Identifier = int.Parse(node["Id"].InnerText),
                    Description = node["Description"].InnerText,
                    Completed = Convert.ToBoolean(node["Completed"].InnerText),
                    Deleted = Convert.ToBoolean(node["Deleted"].InnerText)
                });
            }

            return tasks;
        }

        public IList<ToDoTask> GetTasks()
        {
            IList<ToDoTask> tasks = new List<ToDoTask>();

            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(tasks.GetType());

            System.IO.StreamReader file = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/XMLFileToDoTasks.xml"));
            tasks = (List<ToDoTask>)reader.Deserialize(file);

            file.Close();

            return tasks;
        }
    }
}
