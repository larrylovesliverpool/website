﻿using WebSite.Domain;

namespace WebSite.Orchestration
{
    public interface IAudit
    {
        void Save();

        void Save(Audit auditDetails);
    }
}
