﻿using System;
using System.Collections.Generic;
using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.Respository
{
    public class TrainsConfigurationRepository 
    {
        List<TrainsConfiguration> configurations = new List<TrainsConfiguration>()
        {
            new TrainsConfiguration() { ConfigurationId = 1, Configuration = "0-4-0" },
            new TrainsConfiguration() { ConfigurationId = 2, Configuration = "0-6-0" },
            new TrainsConfiguration() { ConfigurationId = 3, Configuration = "2-6-0" },
            new TrainsConfiguration() { ConfigurationId = 4, Configuration = "4-6-0" },
            new TrainsConfiguration() { ConfigurationId = 5, Configuration = "4-6-2" },
        };

        public List<TrainsConfiguration> Retrieve() => configurations;

        public TrainsConfiguration Retrieve(int key) => configurations.Find(p => p.ConfigurationId == key);

    }
}
