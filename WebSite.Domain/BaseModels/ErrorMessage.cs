﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain.BaseModels
{
    public class ErrorMessage
    {
        private bool _hasError;
        private int _code;
        private string _message;
        private Exception _exception;

        public int Code
        {
            get
            {
                return _code;
            }

            set
            {
                _code = value;
            }
        }

        public string Message
        {
            get
            {
                return _message;
            }

            set
            {
                _message = value;
            }
        }

        public Exception Exception
        {
            get
            {
                return _exception;
            }

            set
            {
                _exception = value;
            }
        }

        public bool HasError
        {
            get
            {
                return _hasError;
            }

            set
            {
                _hasError = value;
            }
        }

        public ErrorMessage ()
        {
            HasError = false;
            Code = 0;
            Message = string.Empty;
            Exception = null;
        }
    }
}
