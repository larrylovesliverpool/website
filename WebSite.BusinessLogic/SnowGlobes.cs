﻿using System;
using System.Collections.Generic;
using WebSite.Domain;
using WebSite.Domain.DataTransferObjects;

namespace WebSite.BusinessLogic
{
    public class SnowGlobes
    {
        //use interface, so program to interface not instance.
        public SnowGlobesDTO GetAll() {

            SnowGlobesDTO snowGlobesDTO = new SnowGlobesDTO();
            snowGlobesDTO.ListOf = new List<SnowGlobeDTO>(); 

            var ListOfSnowGlobes = new WebSite.Respository.SnowGlobes().ReadAll();

            foreach (SnowGlobe snowGlobe in ListOfSnowGlobes) {

                snowGlobesDTO.ListOf.Add(new SnowGlobeDTO() { Name = snowGlobe.Name, Rarity = snowGlobe.Rarity, Comments = snowGlobe.Comments });
            }

            return snowGlobesDTO;
        }
    }
}
