﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.Respository
{
    public class TrainsTypeRepository : ITrainsTypeRepository
    {

        List<Domain.TrainsType> trainTypes = new List<Domain.TrainsType>
        {
            new Domain.TrainsType() { trainsTypeID = 1, type = "Steam" },
            new Domain.TrainsType() { trainsTypeID = 2, type = "Electric" },
            new Domain.TrainsType() { trainsTypeID = 3, type = "Diesel" },
            new Domain.TrainsType() { trainsTypeID = 4, type = "Diesel-Electric" }
        };

        public List<TrainsType> Retrieve() => trainTypes;

        public TrainsType Retrieve(int key) => trainTypes.Find(p => p.trainsTypeID == key);
    }
}
