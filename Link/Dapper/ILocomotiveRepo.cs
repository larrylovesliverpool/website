﻿using System.Collections.Generic;
using WebSite.Domain.Trains;

namespace ServicesInterfaces
{
    public interface ILocomotiveRepo
    {
        // Get all locomotives from database table.
        List<Locomotives> GetLocomotives();

        // get single locomotive from database table.
        Locomotives GetLocomotives(int locoID);

        // Get all locomotives from database inc details, stored procedure.
        List<LocomotiveDetails> GetLocomotivesDetails();

        // Get single locomotive from database inc details, stored procedure.
        LocomotiveDetails GetLocomotivesDetails(int locoID);

        // Get all locomotives from database for GridView, stored procedure.
        List<LocomotivesGridViewDomain> GetGridView();

    }
}