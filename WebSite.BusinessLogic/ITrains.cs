﻿using System.Collections.Generic;
using WebSite.Domain;
using WebSite.Domain.DataTransferObjects;

namespace WebSite.BusinessLogic
{
    public interface ITrains
    {
        List<TrainsDetail> read();

        TrainsDetail read(int key);

        TrainDetailsDTO readAllDetails(int key);
    }
}
