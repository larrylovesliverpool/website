﻿

namespace WebSite.Domain.DataTransferObjects
{
    public class TrainDetailsDTO : BaseModel
    {
        public int trainTypeID { get; set; }
        public string trainType { get; set; }
        public int _LocoId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int manufacturer { get; set; }
        public string decoder { get; set; }
        public int dccValue { get; set; }

        public TrainDetailsDTO() {

            hasData = false;
        }
    }
}