﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class DccDecoder
    {
        public int identifier { get; set; }
        public string manufacturer { get; set; }
        public string description { get; set; }
        public int version { get; set; }
        public int numberOfFunctions { get; set; }
        public string maxCurrentTotal {get;set;}
        public string continuousCurrent { get; set; }
        public string motorCurrent { get; set; }
        public string functionCurrent { get; set; }
        public string addressShort{ get; set; }
        public string addressLong { get; set; }
        public string speedSteps { get; set; }
        public string formFactor { get; set; }
    }
}
