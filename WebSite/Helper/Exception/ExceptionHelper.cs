﻿
using System;
using System.Web.Mvc;

namespace Helper.Exceptions
{
    // general attribute decorator, will override global filter

    public class ExceptionHelperAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var routedata = filterContext.RequestContext.RouteData.Route;

            var routeValues = filterContext.RequestContext.RouteData.Values;

            var controller = routeValues["controller"];
            string action = routeValues["action"].ToString();

            if (!filterContext.ExceptionHandled) {

                // ****************************************************************
                // views/<CurrentControllerName>/Error.cshtml or if not implemented
                // views/shared/Error.cshtml
                // ****************************************************************

                filterContext.Result = new ViewResult()
                {
                    // ** view **
                    //ViewName = "Error"
                    ViewName = action,
                    ViewData = new ViewDataDictionary(filterContext.Exception.Message.ToString())
                };

                filterContext.ExceptionHandled = true;
            }
            

        }
    }

    public class Err : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            System.Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;
            var model = new HandleErrorInfo(filterContext.Exception, "Controller", "Action");

            filterContext.Result = new ViewResult()
            {
                ViewName = "Error1",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}