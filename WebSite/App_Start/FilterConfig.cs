﻿using System.Web;
using System.Web.Mvc;
using WebSite.Helper.Filter;

namespace WebSite
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            // **
            // global filter runs as default exception error
            // **

            //filters.Add(new HandleErrorAttribute());

            // **
            // page footer global filter
            // **

            filters.Add(new FooterMessageAttribute());
        }
    }
}
