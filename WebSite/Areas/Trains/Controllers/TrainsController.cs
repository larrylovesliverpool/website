﻿using System.Web.Mvc;

namespace WebSite.Areas.Trains.Controllers
{
    public class TrainsController : Controller
    {
        private string _controller;

        public string Controller
        {
            get
            {
                return _controller;
            }
        }
    }
}