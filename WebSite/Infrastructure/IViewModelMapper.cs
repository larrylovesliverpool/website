﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Infrastructure
{
    public interface IViewModelMapper<TViewModel>
    {
        TViewModel Load();

        void Save(TViewModel model);

        IEnumerable<TViewModel> GetAll();

    }
}