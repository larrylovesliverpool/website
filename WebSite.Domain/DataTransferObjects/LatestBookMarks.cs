﻿using System;
using System.Collections.Generic;

using WebSite.Domain.BaseModels;

namespace WebSite.Domain.dto
{
    public class LatestBookMarks
    {
        private List<LatestBookMark> _BookMarks;
        private ErrorMessage _ErrorCondition;

        public List<LatestBookMark> BookMarks
        {
            get
            {
                return _BookMarks;
            }

            set
            {
                _BookMarks = value;
            }
        }

        public ErrorMessage ErrorCondition
        {
            get
            {
                return _ErrorCondition;
            }

            set
            {
                _ErrorCondition = value;
            }
        }

        public LatestBookMarks()
        {
            ErrorCondition = new ErrorMessage();
        }
    }
}
