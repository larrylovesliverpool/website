﻿using System.Collections.Generic;
using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.ServiceInterfaces
{
    public interface ITrainsWagon : IRepositoryReader<TrainsWagon> {}
}
