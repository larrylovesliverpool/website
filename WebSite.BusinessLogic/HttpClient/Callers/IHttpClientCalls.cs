﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.BusinessLogic.HttpClients.Callers
{
    public interface IHttpClientCalls
    {
        Task<T1> Get<T1, T2>(string url, string baseAddress, string accessKey, T2 payload) where T2 : class;
    }
}
