﻿using System;
using System.Collections.Generic;
using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.Respository
{
    public class DccDecodersRepository : IDccDecoderRepository
    {
        List<DccDecoder> decoders = new List<DccDecoder>
        {
            new DccDecoder() { identifier = 2, manufacturer = "Hornby", description="R8249",version = 1,formFactor="8 Pin Fly lead" , numberOfFunctions = 4 },
            new DccDecoder() { identifier = 4, manufacturer = "Hornby TTS", description="",version = 1,formFactor="TTS 8 pin Decoder" , numberOfFunctions = 4 },
            new DccDecoder() { identifier = 8, manufacturer = "Bachmann", description="36-553",version = 1,formFactor="21 Pin", numberOfFunctions = 3},
            new DccDecoder() { identifier = 16, manufacturer = "Bachmann", description="36-556",version = 1,formFactor="21 Pin" , numberOfFunctions = 4},
            new DccDecoder() { identifier = 32, manufacturer = "Lais" , description="860018 Ver 1",version = 1,formFactor="8 Pin Fly lead" , numberOfFunctions = 4},
            new DccDecoder() { identifier = 64, manufacturer = "Lais" , description="860021 Ver 3",version = 3,formFactor="8 Pin Direct", numberOfFunctions = 4},
            new DccDecoder() { identifier = 128, manufacturer = "Lais" , description="860021 Ver 4",version = 4,formFactor="8 Pin Fly lead" , numberOfFunctions = 4},
            new DccDecoder() { identifier = 256, manufacturer = "Hattons", description="8 Pin Direct",version = 1, formFactor="8 Pin Direct plug in Decoder", numberOfFunctions = 4},
            new DccDecoder() { identifier = 1024, manufacturer = "GaugeMaster", description="DCC27",version = 1, formFactor="21 & 8 Pin Decoder" , numberOfFunctions = 4}
        };

        public List<DccDecoder> Retrieve() => decoders;

        public DccDecoder Retrieve(int key) => decoders.Find(d => d.identifier == key);
    }
}
