﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Domain.DataTransferObjects;

namespace WebSite.BusinessLogic
{
    public class TrainsCoaches
    {
        ServiceInterfaces.ITrainsCoachesRepository coachesRepo;
        ServiceInterfaces.ITrainsManufacturerRepository manufRepo;

        public TrainsCoaches()
        {
            coachesRepo = new WebSite.Repository.TrainCoachesRepository();
        }

        public TrainsCoaches(ServiceInterfaces.ITrainsCoachesRepository repo)
        {
            coachesRepo = repo;
        }

        public List<Domain.TrainsCoach> read() => coachesRepo.Retrieve();

        public Domain.TrainsCoach read(int key) => coachesRepo.Retrieve(key);

        public TrainCoachesDTO ReadWithDetails()
        {
            TrainCoachesDTO coachesListOf = new TrainCoachesDTO();

            try
            {
                var coaches = coachesRepo.Retrieve();
                var manufs = manufRepo.Retrieve();

                var fulldetails = from coach in coaches
                                  join manuf in manufs on coach.ManufacturerID equals manuf.manufacturerID
                                  select new
                                  {
                                      CoachID = coach.CoachID,
                                      CoachDescription = coach.Description,
                                      manufacturerName = manuf.name,
                                  };

                coachesListOf.hasData = true;
            }
            catch
            {
                coachesListOf.hasData = false;
            }

            return coachesListOf;
        }

        public TrainCoachDTO ReadWithDetails(int key)
        {
            TrainCoachDTO wagonDetails = new TrainCoachDTO();

            try
            {
                var wagons = coachesRepo.Retrieve();
                var manufs = manufRepo.Retrieve();

                var fulldetails = from coach in wagons
                                  join manuf in manufs on coach.ManufacturerID equals manuf.manufacturerID
                                  where coach.CoachID == key
                                  select new
                                  {
                                      coachID = coach.CoachID,
                                      coachDescription = coach.Description,
                                      manufacturerName = manuf.name,
                                  };

                var singleDetail = fulldetails.First();

                wagonDetails.CoachID = singleDetail.coachID;
                wagonDetails.Description = singleDetail.coachDescription;
                wagonDetails.ManufacturerName = singleDetail.manufacturerName;

                wagonDetails.hasData = true;
            }
            catch
            {
                wagonDetails.hasData = false;
            }

            return wagonDetails;
        }

        private TrainCoachesDTO ToDTO()
        {
            TrainCoachesDTO dto = new TrainCoachesDTO();

            return dto;
        }

    }
}
