﻿using WebSite.Domain;

namespace WebSite.ServiceInterfaces
{
    public interface IDccDecoderRepository : IRepositoryReader<DccDecoder> { }
}
