﻿using WebSite.Domain;

namespace WebSite.Infrastructure
{
    public interface IAudit
    {
        void Save();

        void Save(Audit auditDetails);
    }
}
