﻿
$(document).ready(function () {

    //$("button").on('click', function () {

    //    var MVCController = $(this).attr("data-controller");
    //    var MVCController = $("#btnAction").attr("data-controller");
    //    var MVCAction = $("#btnAction").attr("data-action");

    //    var x = $("form").serializeArray();

    //    $.each(x, function (i, field) {
    //        $("#btnAction").append(field.name + ":" + field.value + " ");
    //    });

    //    url = '/Development/' + MVCController + '/' + MVCAction;

    //    $.ajax({
    //        type: "GET",
    //        url: "/Development/" + MVCController + '/' + MVCAction,
    //        contentType: "application/json; charset=utf-8",
    //        data: { identifier: 5 },
    //        dataType: "json",
    //        success: function(rtnData) { 
    //            alert('Success');
    //        },
    //        error: function (xhr,status,errorthrown) {
    //            alert('Error:' + errorthrown);
    //        }
    //    });


    //});

    // event button click, any button clicked in WrapperDemo
    // uses delgation for dynamic loaded sections

    $("#WrapperDemo").on('click', 'button', function (e) {
        e.preventDefault();
        
        // get button action, this will indicate button press 
        // and what action to take
        var buttonAction = $(this).attr("Data-Action");

        switch (buttonAction) {
            case "Clear":
                $("#partialviews").empty();
                break;
            default:
                break;
        }

        e.stopPropagation();
    });

    // ********************************
    // partial view load on click event
    // ********************************

    $("#Partial").on('click',function () {
        $("#partialviews").load('/Development/Home/ActionPartial');
    });

});

