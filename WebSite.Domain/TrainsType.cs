﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class TrainsType
    {
        public int trainsTypeID { get; set; }
        public string type { get; set; }
    }
}
