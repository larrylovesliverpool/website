﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Domain;

namespace Link
{
    public interface ITrack
    {
        Track Read();
    }
}
