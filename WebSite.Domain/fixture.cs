﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class fixture
    {
        private DateTime _schedule;
        private string _homeAway;
        private string _opposition;

        public DateTime Schedule
        {
            get
            {
                return _schedule;
            }

            set
            {
                _schedule = value;
            }
        }

        public string Opposition
        {
            get
            {
                return _opposition;
            }

            set
            {
                _opposition = value;
            }
        }

        public string HomeAway
        {
            get
            {
                return _homeAway;
            }

            set
            {
                _homeAway = value;
            }
        }
    }
}
