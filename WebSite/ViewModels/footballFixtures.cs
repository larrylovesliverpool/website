﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WebSite.Infrastructure;

namespace WebSite.ViewModels
{
    public class footballFixtures
    {

        public int matchDay { set; get; }

        public string homeTeam { set; get; }

        public string awayTeam { set; get; }

    }
}