﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebSite.Domain;

namespace WebSite.ViewModels
{
    public class CoachesPartialVM
    {
        private List<Domain.TrainsCoach> coaches;

        public List<TrainsCoach> Coaches
        {
            get
            {
                return coaches;
            }

            set
            {
                coaches = value;
            }
        }
    }
}