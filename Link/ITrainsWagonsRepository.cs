﻿using WebSite.Domain;

namespace WebSite.ServiceInterfaces
{
    public interface ITrainsWagonsRepository : IRepositoryReader<TrainsWagon> {}
}
