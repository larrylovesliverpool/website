﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.ViewModels.Formula1
{
    public class RacesViewModel
    {
        public string season { get; set; }

        public List<Race> ListOf { get; set; }
    }

    public class Race
    {
        public string round { get; set; }
        public string raceName { get; set; }
        public string Circuit { get; set; }
        public string Country { get; set; }
        public string location { get; set; }
        public string date { get; set; }
        public string time { get; set; }
    }

}