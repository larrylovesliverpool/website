﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebSite.Domain;

namespace WebSite.Controllers
{

    public class WebApiController : ApiController
    {
        // GET: api/WebApi
        public IList<Domain.ToDoTask> Get()
        {
            var tasks = new Respository.ToDoTasksRepository().GetTasks();

            return tasks;
        }

        // GET: api/WebApi/5
        public string Get(int id)
        {
            return "value";
        }

    }
}
