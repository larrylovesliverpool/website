﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using System.Web.Helpers;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;

[assembly: OwinStartup(typeof(WebSite.Startup))]

namespace WebSite
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            AntiForgeryConfig.UniqueClaimTypeIdentifier = "sub";
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap = new Dictionary<string, string>();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies"
            });

            app.UseOpenIdConnectAuthentication(
                new OpenIdConnectAuthenticationOptions
                {
                    ClientId = "LLL",
                    ClientSecret = "ABC123",
                    //Authority = "https://localhost:9002/identity",
                    Authority = "https://simstrek.co.uk/identity",
                    //RedirectUri = "http://localhost:9001/",
                    RedirectUri = "http://www.larrylovesliverpool.uk/",
                    //PostLogoutRedirectUri = "http://localhost:9001/",
                    PostLogoutRedirectUri = "http://www.larrylovesliverpool.uk/",
                    ResponseType = "id_token token",
                    Scope = "openid profile",
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = "name",
                        RoleClaimType = "role"
                    },
                    SignInAsAuthenticationType = "Cookies",
                    UseTokenLifetime = false,
                    Caption = "Larry Loves Beer"
                });
        }
    }
}
