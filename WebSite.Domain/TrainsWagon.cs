﻿

namespace WebSite.Domain
{
    public class TrainsWagon : TrainsRollingStock
    {
        private int _wagonID;
        private int _wagonType;

        public int WagonID
        {
            get
            {
                return _wagonID;
            }

            set
            {
                _wagonID = value;
            }
        }

        public int WagonType
        {
            get
            {
                return _wagonType;
            }

            set
            {
                _wagonType = value;
            }
        }
    }
}
