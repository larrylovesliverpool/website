﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Helper.Exceptions
{
    public class WebSiteException : SystemException
    {
        public int ErrorCode { get; set; }
        public bool Email  { get; set; }

        public WebSiteException() : base() { }
        public WebSiteException(string message) : base(message) { }
        public WebSiteException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected WebSiteException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
        { }
    }
}