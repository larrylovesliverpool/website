﻿
//

using System;
using System.Collections.Generic;
using ServicesInterfaces;
using WebSite.Domain;

using System.Data.SqlClient;
using System.Data;

namespace WebSite.Repository
{
    public class LocomotiveRepository : ILocomotivesRepository
    {
        private DataAccess connection = new DataAccess("DefaultConnection");

        public IList<Locomotive> read()
        {
            IList<Locomotive> locoListOf = new List<Locomotive>();

            SqlCommand command = new SqlCommand("Locomotive_SelectAll", connection.Connection);
            command.CommandType = CommandType.StoredProcedure;       

            connection.Open();

            command = new SqlCommand("Locomotive_SelectAll", connection.Connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Locomotive locoDetails = new Locomotive();

                    locoDetails.Name = reader.GetString(0);
                    locoDetails.Description = reader.GetString(1);

                    locoListOf.Add(locoDetails);
                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }
            reader.Close();

            return locoListOf;
        }

        public Locomotive read(int identifier)
        {
            throw new NotImplementedException();
        }

        public LocomotiveDetails readDetails(int locoId) {

            LocomotiveDetails details = new LocomotiveDetails();

            SqlCommand command = new SqlCommand("LocomotiveDetails", connection.Connection);
            command.CommandType = CommandType.StoredProcedure;

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    details.LocoId = (int)reader["LocoId"];
                    details.LocomotiveName = (string)reader["locomotiveName"];
                    details.Description = (string)reader["Description"];

                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }
            reader.Close();

            return details;
        }
    }
}
