﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.ViewModels
{
    public class PartialViewVM
    {
        public string name { get; set; }
        public string desc { get; set; }
    }
}