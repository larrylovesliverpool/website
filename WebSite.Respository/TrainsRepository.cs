﻿
using System;
using System.Collections.Generic;
using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.Respository
{
    public class TrainsRepository : ITrainsRepository
    {

        List<TrainsDetail> trains = new List<TrainsDetail>
        {
             new TrainsDetail() { Identifier =  1, Name = "Class 08", TrainType = 3, Manufacturer = 1, Description = "Shunter, BR Green", DccValue = 20,Decoder = 32 },
             new TrainsDetail() { Identifier =  2, Name = "Class 20", TrainType = 3, Manufacturer = 2 , Description = "Light mixed traffic" , DccValue = 12,Decoder = 8 },
             new TrainsDetail() { Identifier =  3, Name = "Class 25", TrainType = 3, Manufacturer = 2, Description = "BR Blue", DccValue = 18, Decoder = 8 },
             new TrainsDetail() { Identifier =  4, Name = "Class 35", TrainType = 3, Manufacturer = 1, Description = "Hymek BR Green",DccValue = 17, Decoder = 8 },
             new TrainsDetail() { Identifier =  5, Name = "Class 37", TrainType = 3, Manufacturer = 2, Description = "BR Blue", DccValue = 19, Decoder = 2 },
             new TrainsDetail() { Identifier =  6, Name = "Class 42", TrainType = 3, Manufacturer = 1, Description = "Warship Benbow", DccValue = 27, Decoder = 2 },
             new TrainsDetail() { Identifier =  24, Name = "Class 43", TrainType = 3, Manufacturer = 1 , Description = "Diesel Multi Unit HST, GNER livery", DccValue = 32, Decoder = 2 },
             new TrainsDetail() { Identifier =  7, Name = "Class 43", TrainType = 3, Manufacturer = 1 , Description = "Diesel Multi Unit HST, Virgin livery", DccValue = 33, Decoder = 2 },
             new TrainsDetail() { Identifier =  8, Name = "Class 47", TrainType = 3, Manufacturer = 1 , Description = "Rail-freight", DccValue = 16, Decoder = 2 },
             new TrainsDetail() { Identifier =  9, Name = "Class 47", TrainType = 3, Manufacturer = 1, Description = "Virgin", DccValue = 29, Decoder = 2 },
             new TrainsDetail() { Identifier = 10, Name = "Class 52", TrainType = 3, Manufacturer = 1 , Description = "Western (Maroon)", DccValue = 28, Decoder = 2 },
             new TrainsDetail() { Identifier = 11, Name = "Class 55", TrainType = 3, Manufacturer = 1, Description = "Deltic BR blue", DccValue = 21, Decoder = 2 },
             new TrainsDetail() { Identifier = 12, Name = "Class 55", TrainType = 3, Manufacturer = 1, Description = "Deltic BR green", DccValue = 23, Decoder = 2 },
             new TrainsDetail() { Identifier = 13, Name = "Class 66", TrainType = 3, Manufacturer = 2, Description = "GBRF", DccValue = 10, Decoder = 2 },
             new TrainsDetail() { Identifier = 14, Name = "Class 108", TrainType = 3, Manufacturer = 2 , Description = "Diesel Multi Unit", DccValue = 14, Decoder = 2 },
             new TrainsDetail() { Identifier = 15, Name = "Class 156", TrainType = 3, Manufacturer = 2 , Description = "Diesel Multi Unit,", DccValue = 22, Decoder = 2 },
             new TrainsDetail() { Identifier = 23, Name = "Class 295", TrainType = 3, Manufacturer = 1 , Description = "Electric Multi Unit,", DccValue = 40, Decoder = 2 },
             new TrainsDetail() { Identifier = 16, Name = "Evening Star", TrainType = 1, Manufacturer = 1 , Description = "Steam 2-8-0 BR Green", DccValue = 11, Decoder = 2  },
             new TrainsDetail() { Identifier = 17, Name = "Flying Scottsmans", TrainType = 1, Manufacturer = 1 , Description = "Steam 4-6-2 LNER Apple green", DccValue = 26, Decoder = 2 },
             new TrainsDetail() { Identifier = 18, Name = "Duke of Gloucester", TrainType = 3, Manufacturer = 1 , Description = "Steam 4-6-2 BR Green", DccValue = 15, Decoder = 2 },
             new TrainsDetail() { Identifier = 19, Name = "0-4-0T", TrainType = 1, Manufacturer = 1 , Description = "Steam Shunter/light traffic", DccValue = 25, Decoder = 32 },
             new TrainsDetail() { Identifier = 20, Name = "Britannia", TrainType = 1, Manufacturer = 1 , Description = "Steam 4-6-2 BR Green, 1970's", DccValue = 45, Decoder = 2 },
             new TrainsDetail() { Identifier = 21, Name = "Gladwell", TrainType = 1, Manufacturer = 1 , Description = "Steam 4-6-2 A4,LNER Blue, TTS sound decoder", DccValue = 34, Decoder = 2 },
             new TrainsDetail() { Identifier = 22, Name = "Windhoff MPV", TrainType = 4, Manufacturer = 1 , Description = "Diesel, MPV", DccValue = 35, Decoder = 4 },
             new TrainsDetail() { Identifier = 23, Name = "Class 59", TrainType = 3, Manufacturer = 1 , Description = "EWS Diesel", DccValue = 41, Decoder = 4 },
             new TrainsDetail() { Identifier = 25, Name = "Class 128", TrainType = 3, Manufacturer = 1 , Description = "Parcel DMU", DccValue = 31, Decoder = 4 },
             new TrainsDetail() { Identifier = 26, Name = "Drumond", TrainType = 1, Manufacturer = 1 , Description = "Steam 0-6-0,Tender, BR black", DccValue = 38, Decoder = 2 },
             new TrainsDetail() { Identifier = 27, Name = "Class 92", TrainType = 3, Manufacturer = 1 , Description = "Electric", DccValue = 37, Decoder = 1 },
        };

        public List<TrainsDetail> Retrieve() => trains;

        public TrainsDetail Retrieve(int key) => trains.Find(i => i.Identifier == key);
    }
}
