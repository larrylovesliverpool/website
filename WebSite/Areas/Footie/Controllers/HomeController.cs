﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Areas.Footie.Controllers
{
    public class HomeController : Controller
    {
        // GET: Footie/Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Bookmarks()
        {
            ViewBag.Area = "Football";
            return View();
        }
    }
}