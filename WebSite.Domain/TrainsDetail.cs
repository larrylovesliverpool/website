﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class TrainsDetail
    {
        private int _Identifier;
        private string _name;
        private int _trainType;
        private string _description;
        private int _manufacturer;
        private int _decoder;
        private int _dccValue;
        private int _classification;
        private int _configuration;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public int Manufacturer
        {
            get
            {
                return _manufacturer;
            }

            set
            {
                _manufacturer = value;
            }
        }

        public int Decoder
        {
            get
            {
                return _decoder;
            }

            set
            {
                _decoder = value;
            }
        }

        public int DccValue
        {
            get
            {
                return _dccValue;
            }

            set
            {
                _dccValue = value;
            }
        }

        public int Identifier
        {
            get
            {
                return _Identifier;
            }

            set
            {
                _Identifier = value;
            }
        }

        public int TrainType
        {
            get
            {
                return _trainType;
            }

            set
            {
                _trainType = value;
            }
        }

        public int Classification
        {
            get
            {
                return _classification;
            }

            set
            {
                _classification = value;
            }
        }

        public int Configuration
        {
            get
            {
                return _configuration;
            }

            set
            {
                _configuration = value;
            }
        }
    }
}
