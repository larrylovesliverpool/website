﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.ViewModels
{
    public class OUGlossary
    {
        private string _name;
        private string _description;
        private int _topic;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public int Topic
        {
            get
            {
                return _topic;
            }

            set
            {
                _topic = value;
            }
        }
    }
}