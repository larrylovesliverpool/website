﻿

/* plug example */

(function ($) {

    $.fn.helloWorld = function ( options) {

        // Establish our default settings
        var settings = $.extend({
            text: 'Default Message.',
            color: 'blue',
            fontStyle: null,
            complete: null,
            success: null,
            error: null
        }, options);

        return this.each(function () {
            $(this).append(settings.text);

            $(this).css('color', settings.color);

            if ($.isFunction(settings.complete)) {
                settings.complete.call(this);
            }
        });

    }

    $.fn.Broadcast = function (options) {

        // Establish our default settings
        var settings = $.extend({
            text: 'Default Message, No message text.',
            color: 'blue',
            bootstrap: null,
            complete: null,
            success: null,
            error: null,

        }, options);

        return this.each(function () {
            $(this).append(settings.text);

            $(this).css('color', settings.color);

            if ($.isFunction(settings.complete)) {
                settings.complete.call(this);
            }


        });

    }

}(jQuery));

/* EOF */