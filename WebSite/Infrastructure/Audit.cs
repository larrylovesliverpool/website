﻿using System;
using WebSite.Domain;

namespace WebSite.Infrastructure
{
    public class Audit<T> : IAudit where T : class, new() 
    {
        private T _model;
        IAudit auditTasks;

        public T Model
        {
            get { return _model; }
            set { _model = value; }
        }

        #region "Constructors"

        public Audit() { }

        public Audit(T t) {
            Model = t;
        }

        public Audit(IAudit audit)
        {
            auditTasks = audit;
        }

        public Audit( IAudit audit, T t)
        {
            auditTasks = audit;
            Model = t;
        }

        #endregion

        #region "Methods"

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Add(T node)
        {
            Model = node;
        }


        public override string ToString()
        {
            return base.ToString();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Save(Audit auditDetails)
        {
            throw new NotImplementedException();
        }

        #endregion

    }

    public static class Log<T>
    {

        public static void Save(T entry)
        {
            // Convert entry into XML !!!
            // Save entry into log
        }

        public static void Save(Audit LogDetails,
                                T entry)
        {
            // Convert entry into XML !!!
            // Save entry into log

            //LogDetails.Description = ;
        }


        public static void Save()
        {
        }

        public static void Save(string xml)
        {
        }


    }

}