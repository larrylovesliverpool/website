﻿using WebSite.Respository;
using WebSite.ServiceInterfaces;
using WebSite.Domain.DataTransferObjects;
using System.Linq;
using WebSite.Domain;
using System.Collections.Generic;

namespace WebSite.BusinessLogic
{
    public class Trains : ITrains
    {
        ITrainsRepository trainsRepo;
        ITrainsTypeRepository trainTypeRepo;
        IDccDecoderRepository dccDecoderRepo;

        public Trains()
        {
            trainsRepo = new TrainsRepository();
            trainTypeRepo = new TrainsTypeRepository();
            dccDecoderRepo = new DccDecodersRepository();
        }

        public List<TrainsDetail> read() => trainsRepo.Retrieve();

        public Domain.TrainsDetail read(int key) => trainsRepo.Retrieve(key);

        public TrainDetailsDTO readAllDetails(int key) {

            TrainDetailsDTO trainDetails = new Domain.DataTransferObjects.TrainDetailsDTO();

            try
            {
                var trains = trainsRepo.Retrieve();
                var trainTypes = trainTypeRepo.Retrieve();
                var decoders = dccDecoderRepo.Retrieve();

                var fulldetails = from train in trains
                                  join trainType in trainTypes on train.TrainType equals trainType.trainsTypeID
                                  join decoder in decoders on train.Decoder equals decoder.identifier
                                  where train.Identifier == key
                                  select new
                                  {
                                      name = train.Name,
                                      manufacturer = train.Manufacturer,
                                      description = train.Description,
                                      trainType = trainType.type,
                                      dccValue = train.DccValue,
                                      decoder = decoder.manufacturer + " " + decoder.description
                                  };

                var singleDetail = fulldetails.First();

                trainDetails.name = singleDetail.name;
                trainDetails.manufacturer = singleDetail.manufacturer;
                trainDetails.trainType = singleDetail.trainType;
                trainDetails.description = singleDetail.description;
                trainDetails.dccValue = singleDetail.dccValue;
                trainDetails.decoder = singleDetail.decoder;

                trainDetails.hasData = true;
            }
            catch
            {
                trainDetails.hasData = false;
            }

            return trainDetails;
        }

    }
}
