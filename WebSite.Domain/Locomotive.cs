﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public class Locomotive : TrainsRollingStock
    {
        private int _LocoId;
        private string _name;
        private int _trainType;
        private string _decoder;
        private int _ddcValue;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Decoder
        {
            get
            {
                return _decoder;
            }

            set
            {
                _decoder = value;
            }
        }

        public int DdcValue
        {
            get
            {
                return _ddcValue;
            }

            set
            {
                _ddcValue = value;
            }
        }

        public int LocoId
        {
            get
            {
                return _LocoId;
            }

            set
            {
                _LocoId = value;
            }
        }

        public int TrainType
        {
            get
            {
                return _trainType;
            }

            set
            {
                _trainType = value;
            }
        }
    }
}
