﻿using System;
using WebSite.Domain;

namespace WebSite.ViewModels
{
    public class CoachesVM : BaseModels.ErrorCondition, IViewModel<CoachesVM>
    {


        public CoachesVM generateViewModel()
        {
            BusinessLogic.TrainCoaches coaches = new BusinessLogic.TrainCoaches();
            var data = coaches.read();

            return this;
        }
    }
}