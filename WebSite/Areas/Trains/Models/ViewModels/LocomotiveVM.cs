﻿using AutoMapper;
using System.ComponentModel.DataAnnotations;
using WebSite.Domain;
using WebSite.Domain.DataTransferObjects;
using WebSite.ViewModels.BaseModels;

namespace WebSite.Areas.Trains.ViewModels
{
    public class LocomotiveVM : ErrorCondition
    {
        private int _LocoId;
        private string _name;
        private string _description;
        private int _manufacturer;
        private string _decoder;
        private int _dccValue;
        private string _trainType;
        private bool _stayALive;
        private string _Comments;

        [Required]
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public int Manufacturer
        {
            get
            {
                return _manufacturer;
            }

            set
            {
                _manufacturer = value;
            }
        }

        public string Decoder
        {
            get
            {
                return _decoder;
            }

            set
            {
                _decoder = value;
            }
        }

        public int DccValue
        {
            get
            {
                return _dccValue;
            }

            set
            {
                _dccValue = value;
            }
        }

        public int LocoId
        {
            get
            {
                return _LocoId;
            }

            set
            {
                _LocoId = value;
            }
        }

        public string TrainType
        {
            get
            {
                return _trainType;
            }

            set
            {
                _trainType = value;
            }
        }

        public bool StayALive { get => _stayALive; set => _stayALive = value; }
        public string Comments { get => _Comments; set => _Comments = value; }

        //public LocomotiveVM generateViewModel()
        //{
        //    return this;
        //}

        //public LocomotiveVM generateViewModel(int identifier)
        //{
        //    BusinessLogic.Trains trainDetails = new BusinessLogic.Trains();
        //    var data = trainDetails.readAllDetails(identifier);

        //    if (data.hasData)
        //    {
        //        this.Name = data.name;
        //        this.Manufacturer = data.manufacturer;
        //        this.Description = data.description;
        //        this.DccValue = data.dccValue;
        //        this.TrainType = data.trainType;
        //        this.Decoder = data.decoder;
        //    }
        //    else
        //    {
        //        this.HasError = true;
        //        this.ErrorMessage = "Error reading data";
        //    }

        //    return this;
        //}

        public static implicit operator LocomotiveVM(Locomotive locomotive)
        {
            return Mapper.Map<LocomotiveVM>(locomotive);
        }

        /* implicit conversion, eleminates conversion/casting 
           view model becomes equal to contents of DTO, using mapper to match properties */

        public static implicit operator LocomotiveVM(LocomotiveDTO locomotive)
        {
            return Mapper.Map<LocomotiveDTO>(locomotive);
        }
    }
}