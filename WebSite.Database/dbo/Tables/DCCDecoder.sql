﻿CREATE TABLE [larry].[DCCDecoder] (
    [DecoderId]    INT           NOT NULL IDENTITY,
    [Manufacturer] NVARCHAR (50) NULL,
    [Model]        NVARCHAR (50) NULL, 
    CONSTRAINT [PK_DCCDecoder] PRIMARY KEY ([DecoderId])
);

