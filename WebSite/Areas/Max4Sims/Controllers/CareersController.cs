﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Areas.Max4Sims.Controllers
{
    public class CareersController : Controller
    {
        // GET: Max4Sims/Careers
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Careers()
        {
            return View();
        }

        public ActionResult Astronaut()
        {
            return View();
        }

        public ActionResult Athlete()
        {
            return View();
        }

        public ActionResult Business() => View();

        public ActionResult Criminal() => View();

        public ActionResult Critic() => View();

        public ActionResult Culinary() => View();

        public ActionResult Entertainer() => View();

        public ActionResult Painter() => View();

        public ActionResult Politician() => View();

        public ActionResult SecretAgent() => View();

        public ActionResult SocialMedia() => View();

        public ActionResult TechGuru() => View();

        public ActionResult Writer() => View();

        public ActionResult Detective() => View();

        public ActionResult Doctor() => View();

        public ActionResult Scientist() => View();
    }
}