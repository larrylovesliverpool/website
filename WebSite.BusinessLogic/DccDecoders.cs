﻿using System.Collections.Generic;
using WebSite.Domain;
using WebSite.Respository;
using WebSite.ServiceInterfaces;

namespace WebSite.BusinessLogic
{
    public class DccDecoders
    {
        IDccDecoderRepository DccDecoderRepo;

        public DccDecoders()
        {
            DccDecoderRepo = new DccDecodersRepository();
        }

        public DccDecoders(IDccDecoderRepository repo)
        {
            DccDecoderRepo = repo;
        }

        public List<DccDecoder> Read() => DccDecoderRepo.Retrieve();

        public DccDecoder Read(int key) => DccDecoderRepo.Retrieve(key);
    }
}
