﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public abstract class TrainsRollingStockDTO : BaseModelDTO
    {
        private string _productCode;
        private int _manufacturerID;
        private string _description;
        private int _quantity;

        public string ProductCode
        {
            get
            {
                return _productCode;
            }

            set
            {
                _productCode = value;
            }
        }

        public int ManufacturerID
        {
            get
            {
                return _manufacturerID;
            }

            set
            {
                _manufacturerID = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public int Quantity
        {
            get
            {
                return _quantity;
            }

            set
            {
                _quantity = value;
            }
        }
    }
}
