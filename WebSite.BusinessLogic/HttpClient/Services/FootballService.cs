﻿using System.Configuration;
using System.Threading.Tasks;
using WebSite.BusinessLogic.HttpClients.Clients;
using WebSite.Domain.Football.MatchDay;

namespace WebSite.BusinessLogic.HttpClients.Services
{
    public class FootballService
    {
        private string _baseAddress;
        private string _securityToken;

        public string BaseAddress
        {
            get => _baseAddress;
            set => _baseAddress = value;
        }

        public string SecurityToken
        {
            get => _securityToken;
            set => _securityToken = value;
        }

        public FootballService()
        {
            BaseAddress = ConfigurationManager.AppSettings["footballAPI"].ToString();
            SecurityToken = ConfigurationManager.AppSettings["X-Auth-Token"].ToString();
        }

        #region team

        public MatchDays GetMatchDays()
        {
            return Task_GetMatchDays().Result;
        }

        private async Task<MatchDays> Task_GetMatchDays()
        {
            var serviceurl = $"competitions/2021/matches";
            string payload = null;

            return await Task.Run(() => {
                return HttpClientFootball.Service.Caller.Get<MatchDays, string>(serviceurl, BaseAddress, SecurityToken, payload).Result;
            }).ConfigureAwait(continueOnCapturedContext: false);
        }

        #endregion
    }
}
