﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Domain.DataTransferObjects;

namespace WebSite.BusinessLogic
{
    interface IFixtures
    {
        FixtureDTO GetHomeFixtures();

        FixtureDTO GetAwayFixtures();

        FixtureDTO GetNextFixtures();

        IList<FixtureDTO> GetAllFixtures();
    }
}
