﻿
using System.Collections.Generic;
using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.Respository
{
    public interface IDccCvDetailsRepository : IRepositoryReader<DccCvDetails> {

        List<DccCvDetails> RetrieveDecoders(int decoderID);
    }
}
