﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Infrastructure
{
    public interface IGenericExample
    {
        int count {
            get;set;
        }

        void Add();

        void Remove();

        void Sort();

        void Shuffle();
    }
}
