﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Domain.DataTransferObjects;
using WebSite.Respository;

namespace WebSite.BusinessLogic
{
    public class LFCFixtures
    {
        IFixtures fixtures;

        public FixtureDTO GetNextFixtures {

            get { return fixtures.GetNextFixtures(); }
        }

        public LFCFixtures()
        {       
            fixtures = new Fixtures(new LfcFixtures201819());
        }


    }
}
