﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain.Trains
{
    // **************************
    // model for stored procedure : Locomotive_SelectAll
    // **************************

    public class LocomotivesSelectAll
    {
        public int LocoId { get; set; }
        public string Name { get; set; }
        public int MadeBy { get; set; }
        public int Decoder { get; set; }
        public int DCCValue { get; set; }
        public string Description { get; set; }
        public int YearPurchased { get; set; }
        public int Classification { get; set; }
        public string ProcductCode { get; set; }
        public int EraModelled { get; set; }
    }
}
