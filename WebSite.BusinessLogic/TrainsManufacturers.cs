﻿using System.Collections.Generic;
using WebSite.Respository;
using WebSite.ServiceInterfaces;

namespace WebSite.BusinessLogic
{
    public class TrainsManufacturers
    {
        ITrainsManufacturerRepository trainManufacturerRepository;

        public TrainsManufacturers()
        {
            trainManufacturerRepository = new TrainsManufacturerRepository();
        }

        public TrainsManufacturers(TrainsManufacturerRepository repo)
        {
            trainManufacturerRepository = repo;
        }

        public List<Domain.TrainsManufacturer> read() => trainManufacturerRepository.Retrieve();
    }
}
