﻿
$(function () {

    $('#WeatherThirdRow').hide();

    /* Trap ajax errors */
    $(document).ajaxError(function (event, xhr, options, exc) {
        //alert("An error occurred while loading a data feed!  " + xhr.status);
    });

    /*  display a forecast summary 
        for east midlands in text format */
    $('#ForecastSummary').bind('click', function () {

        // toggle open/close for text summary
        if ( $('#PanelForecastDayNight').hasClass('hide') )
        {
            $('#PanelForecastSummary').removeClass('show').addClass('hidden');
            $('#PanelForecastDayNight').removeClass('hide');
            $('#PanelWeather').css('height', '260');
        }
        else {
            $('#PanelForecastDayNight').addClass('hide');
            $('#PanelForecastSummary').removeClass('hidden').addClass('show');
            getWeatherTextData(true);
        }

    });

    /* display time */
    var myVar = setInterval(function () {
        ShowDateTime()
    }, 1000);

    /* display weather for day & night, standard homepage start display */
    var data = getWeatherData(true);

    $('#ForecastDaysAdvance').bind('click', function () {
        $("#DefaultThirdRow").hide(500);
        $("#WeatherThirdRow").show(500);

        $.getJSON('http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/352145?res=daily&key=b16be0af-fe28-4649-ad51-5260d078588c', function (result) {

            weatherData = result;

                //var timeStamp = data.SiteRep.DV.dataDate;
                //$('#forecastTime').append(timeStamp);

                /* ********************************* */
                /* Period == day 1 to 5              */
                /* Rep 1 = daytime 2 = Evening night */
                /* ********************************* */

                for (i = 0; i < 5; i++) {

                    var today = weatherData.SiteRep.DV.Location.Period[i].Rep[0];
                    var tonight = weatherData.SiteRep.DV.Location.Period[i].Rep[1];

                    today.WDescription = getWeatherType(today.W);
                    tonight.WDescription = getWeatherType(tonight.W);

                    $('#weatherToday_' + i).replaceWith(today.Dm + '&deg;C ' + today.S + 'mph' + ' ' + today.D);
                    $('#weatherTodaySymbol_' + i).attr('src', '/content/Images/weather/' + "w" + today.W + ".png");
                    $('#weatherToNight_' + i).replaceWith(tonight.Nm + '&deg;C ' + tonight.S + 'mph' + ' ' + tonight.D);
                    $('#weatherTonightSymbol_' + i).attr('src', '/content/Images/weather/' + "w" + tonight.W + ".png");
                }
        });

    });

    $('#ForecastDaysAdvanceClose').bind('click', function () {
        $("#WeatherThirdRow").hide(250);
        $("#DefaultThirdRow").show(250);
    });

    var leagueTable = getPremierLeagueTable(false);
});

function getWeatherType(value) {

    var weatherType = {};
    weatherType.description = "NA";
    weatherType.symbol = "w" + value + ".png";

    var value = parseInt(value);

    switch (value)
    {
        
        case 0: description = "Clear Night";
            break;
        case 1: description = "Sunny day";
            break;
        case 2: description = "Partly Cloudy (night)";
            break;
        case 3: description = "Partly Cloudy (day)";
            break;
        case 4: description = "Not Used";
            break;
        case 5: description = "Mist";
            break;
        case 6: description = "Fog";
            break;
        case 7: description = "Cloudy";
            break;
        case 8: description = "Overcast";
            break;
        case 9: description = "Light rain shower (night)";
            break;
        case 10: description = "Light rain shower (day)";
            break;
        case 11: description = "Drizzle";
            break;
        case 12: description = "Light rain";
            break;
        case 13: description = "Heavy rain (night)";
            break;
        case 14: description = "Heavy rain (day)";
            break;
        case 15: description = "Heavy rain";
            break;
        case 16: description = "Sleet shower (night)";
            break;
        case 17: description = "Sleet shower (day)";
            break;
        case 18: description = "Sleet";
            break;
        case 19: description = "Hail shower (night)";
            break;
        case 20: description = "Hail shower (day)";
            break;
        case 21: description = "Hail";
            break;
        case 22: description = "Light snow shower (night)";
            break;
        case 23: description = "Light snow shower (day)";
            break;
        case 24: description = "Light snow";
            break;
        case 25: description = "Heavy snow shower (night)";
            break;
        case 26: description = "Heavy snow shower (day)";
            break;
        case 27: description = "Heavy Snow";
            break;
        case 28: description = "Thunder shower (Night)";
            break;
        case 29: description = "Thunder shower (day)";
            break;
        case 30: description = "Thunder";
            break;
        default: description = "Unknown";
    };

    return description;
}

function getWeatherData(display) {

    var data = {};

    $.getJSON('http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/352145?res=daily&key=b16be0af-fe28-4649-ad51-5260d078588c', function (result) {

        data = result;

        if (display) {
            var timeStamp = data.SiteRep.DV.dataDate;
            $('#forecastTime').append(timeStamp);

            /* ********************************* */
            /* Period == day 1 to 5              */
            /* Rep 1 = daytime 2 = Evening night */
            /* ********************************* */

            var today = data.SiteRep.DV.Location.Period[0].Rep[0];
            var tonight = data.SiteRep.DV.Location.Period[0].Rep[1];

            today.WDescription = getWeatherType(today.W);
            tonight.WDescription = getWeatherType(tonight.W);

            $('#weatherToday').append(today.Dm + '&deg;C ' + today.S + 'mph' + ' ' + today.D);
            $('#weatherTodaySymbol').attr('src', '/content/Images/weather/' + "w" + today.W + ".png");
            $('#weatherToNight').append(tonight.Nm + '&deg;C ' + tonight.S + 'mph' + ' ' + tonight.D);
            $('#weatherTonightSymbol').attr('src', '/content/Images/weather/' + "w" + tonight.W + ".png");
        }
    });

    return data;
}

function getWeatherTextData(display) {

    var data = {};

    $.getJSON('http://datapoint.metoffice.gov.uk/public/data/txt/wxfcs/regionalforecast/json/512?%20key=b16be0af-fe28-4649-ad51-5260d078588c', function (result) {

        data = result;

        if (display) {
            //display data on page
            var length = data.RegionalFcst.FcstPeriods.Period[0].Paragraph.length;
            var text = '';
            var i;

            for (i = 0; i < length; i++) {
                text += '<strong>' + data.RegionalFcst.FcstPeriods.Period[0].Paragraph[i].title + '</strong>';
                text += '<br /><br/ >';
                text += data.RegionalFcst.FcstPeriods.Period[0].Paragraph[i].$;
                text += '<br /><br />';
            }
            $('#PanelWeather').css('height','500');
            $('#ForecastSummaryText').empty().append(text);

            $('#ForecastSummaryClose').on('click', function () {
                $('#PanelForecastSummary').addClass('hidden');
                $('#PanelForecastDayNight').removeClass('hide');
                $('#PanelWeather').css('height', '260');
            });

        }

    });

    return data;
}

function getFixturesTextData(display) {

    var data = {};

    $.ajax({
        headers: { 'X-Auth-Token': '7b3e3a54fe7542208e6519b1ff77364c' },
        url: 'http://api.football-data.org/v1/teams/64/fixtures',
        dataType: 'json',
        type: 'GET',
    }).done(function (response) {

        if (display) {

            var htmlstrg;

            for (i = 0; i < response.count; i++) {

                /* form html */

                htmlstrg = '<tr id="tableRowID' + response.fixtures[i].matchday + '" style="display:block;">';
                htmlstrg += '<td id="tablePrev" style="width:15%;"></td>';

                htmlstrg += '<td id="tableFixtures" style="width:70%;">';
                    //htmlstrg += '<p>';
                    htmlstrg += response.fixtures[i].homeTeamName + ' v ' + response.fixtures[i].awayTeamName;
                    //htmlstrg += '</p>';
                    //htmlstrg += '<p>';
                    htmlstrg += response.fixtures[i].date + '. Match day ' + response.fixtures[i].matchday;
                    //htmlstrg += '</p>';
                htmlstrg += '</td>';

                htmlstrg += '<td id="tableNext" style="width:15%;"></td>';
                htmlstrg += '</tr>';

                /* append html */
                $("#TableFixtures").append(htmlstrg);
            }

            //var search = JSON.search(response, '//*[contains(homeTeamName,"Liverpool FC")]');

            //for (var i = 0; i < search.length; i++) {
            //    alert(search[i].homeTeamName + ' v ' + search[i].awayTeamName);
            //}
        }

        data = response;
    });

    return data;
}

function getPremierLeagueTable(display) {

    var data = {};

    $.ajax({
        headers: { 'X-Auth-Token': '7b3e3a54fe7542208e6519b1ff77364c' },
        url: 'http://api.football-data.org/v2/competitions/2021/standings',
        dataType: 'json',
        type: 'GET',
        async: false,
        success: function (response) {

            /* Object */
            leaguetable = {};

            leaguetable.standings = response.standings[0].table;
            leaguetable.name = response.competition.name;
            leaguetable.lastUpdated = response.competition.lastUpdated;
            leaguetable.currentMatchDay = response.season.currentMatchday;
            leaguetable.startDate = response.season.startDate;
            leaguetable.endDate = response.season.endDate;

            data = leaguetable;
        },
        error: function () { },
        complete: function () { },

    });

    return data;
}

function getNextFixture()
{
    // filter json array for liverpool only.
    var search = JSON.search(footballfixturesCalendar, '//*[contains(SUMMARY,"Liverpool")]');
    // field index name
    var matchdate = 'DTSTART;TZID=Europe/London';
    // current datetime in secs
    var now = Date.now();

    for (var i = 0; i < search.length; i++) {

        // get match date in yyyy-mm-dd format and seperate kick off time
        var matchDay = search[i][matchdate].substring(0, 4) + '-' + search[i][matchdate].substring(4, 6) + '-' + search[i][matchdate].substring(6, 8);
        var matchTime = search[i][matchdate].substring(9, 13);


        var d = new Date(matchDay);
        var n = d.toDateString();
        var secs = Date.parse(matchDay);
        var matchsecs = Date.parse(search[i][matchdate]);//2011-10-10T14:48:00"

        // match date passed now so must be next match.
        if (secs >= now) {

            var linesOfText = search[i].DESCRIPTION.split('\\n');

            htmlstrg = '<tr id="tableRowID' + i + '" >';
            htmlstrg += '<td id="tablePrev" style="width:15%;"></td>';

            htmlstrg += '<td id="tableFixtures" style="width:70%;text-align:center;font-size:x-large;">';
            htmlstrg += '<strong>' + linesOfText[0] + '</strong><br />';
            htmlstrg += linesOfText[1] + '<br />';
            htmlstrg += n + ' at ' + matchTime + '<br />';
            htmlstrg += '<br />';
            htmlstrg += '</td>';

            htmlstrg += '<td id="tableNext" style="width:15%;"></td>';
            htmlstrg += '</tr>';

            /* append html */
            $("#TableFixtures").append(htmlstrg);

            // exit loop as only displaying single match.
            break;
        }
    }
}

function ShowDateTime() {
    var d = new Date();
    $("#dateTime").html(d.toLocaleTimeString());
}

/* eof */