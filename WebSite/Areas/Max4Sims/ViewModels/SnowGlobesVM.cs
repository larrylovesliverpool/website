﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Areas.Max4Sims.ViewModels
{
    public class SnowGlobeVM
    {
        private string _name;
        private string _rarity;
        private string _comments;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Rarity
        {
            get
            {
                return _rarity;
            }

            set
            {
                _rarity = value;
            }
        }

        public string Comments
        {
            get
            {
                return _comments;
            }

            set
            {
                _comments = value;
            }
        }
    }

    public class SnowGlobesVM
    {
        public List<SnowGlobeVM> SnowGlobes { get; set; }
    }
}