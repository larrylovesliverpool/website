﻿using Dapper;
using ServicesInterfaces;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using WebSite.Domain.Trains;

namespace WebSite.Repository.Dapper
{
    public class LocomotiveRepo : ILocomotiveRepo
    {
        private readonly string _connectString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public string ConnectString {
            get => _connectString;
        }

        public List<Locomotives> GetLocomotives()
        {
            List<Locomotives> locomotives = new List<Locomotives>();

            using (IDbConnection db = new SqlConnection(ConnectString))
            {
                var resultSet = db.Query<Domain.Trains.Locomotives>(
                    "Locomotive_SelectAll",
                    null,
                    null,
                    true,
                    null,
                    CommandType.StoredProcedure);

                locomotives = resultSet.AsList();
            }

            return locomotives;
        }

        public Locomotives GetLocomotives(int locoID)
        {
            throw new System.NotImplementedException();
        }

        public List<LocomotiveDetails> GetLocomotivesDetails()
        {
            throw new System.NotImplementedException();
        }

        public LocomotiveDetails GetLocomotivesDetails(int locoID)
        {
            List<LocomotiveDetails> details = new List<LocomotiveDetails>();
            Domain.Trains.LocomotiveDetails detail = new Domain.Trains.LocomotiveDetails();

            var queryParameters = new DynamicParameters();
            queryParameters.Add("@LocoID", locoID);

            using (IDbConnection db = new SqlConnection(ConnectString))
            {
                var resultSet = db.Query<LocomotiveDetails>(
                    "Locomotive_SelectWithDetails",
                    queryParameters,
                    null,
                    true,
                    null,
                    CommandType.StoredProcedure);

                details = resultSet.AsList();
            }

            return details[0];
        }

        public List<LocomotivesGridViewDomain> GetGridView()
        {
            List<LocomotivesGridViewDomain> locomotives = new List<LocomotivesGridViewDomain>();

            using (IDbConnection db = new SqlConnection(ConnectString))
            {
                var resultSet = db.Query<LocomotivesGridViewDomain>(
                    "Locomotive_SelectGridView",
                    null,
                    null,
                    true,
                    null,
                    CommandType.StoredProcedure);

                locomotives = resultSet.AsList();
            }

            return locomotives;
        }

        public int Insert(Domain.LocomotiveDetails locomotive)
        {
            var queryParameters = new DynamicParameters();
            queryParameters.Add("@Name", locomotive.LocomotiveName);
            queryParameters.Add("@MadeBy",locomotive.LocoId);
            queryParameters.Add("@Decoder", locomotive.Decoder);
            queryParameters.Add("@DCCValue", locomotive.DccValue);
            queryParameters.Add("@Description", locomotive.Description);
            queryParameters.Add("@YearPurchased", locomotive.YearPurchased);
            queryParameters.Add("@Classification", locomotive.LocomotiveType);
            queryParameters.Add("@ProductCode", locomotive.Type);
            queryParameters.Add("@Eramodelled", 0);

            using (IDbConnection db = new SqlConnection(ConnectString))
            {
                db.Open();
                return db.Execute("Locomotive_Insert",
                           queryParameters,
                           null,
                           null,
                           CommandType.StoredProcedure);
            }
        }

        public bool Delete(int locoId)
        {
            return false;
        }

        public bool update()
        {
            return false;
        }
    }
}
