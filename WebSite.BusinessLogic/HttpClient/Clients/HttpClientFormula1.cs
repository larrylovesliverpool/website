﻿using System;
using WebSite.BusinessLogic.HttpClients;
using WebSite.BusinessLogic.HttpClients.Callers;

namespace WebSite.BusinessLogic.HttpClients.Clients
{
    public class HttpClientFormula1
    {

        private IHttpClientServiceCall _httpClientServiceCall;
        private readonly string _name = "Formula1 Client";

        public IHttpClientServiceCall HttpClientServiceCall
        {
            get { return _httpClientServiceCall; }
            set { _httpClientServiceCall = value; }
        }

        public IHttpClientServiceCall Caller
        {
            get { return _httpClientServiceCall; }
            set { _httpClientServiceCall = value; }
        }

        public string Name
        {
            get { return _name; }
        }

        private static readonly Lazy<HttpClientFormula1> lazy = new Lazy<HttpClientFormula1>(() => new HttpClientFormula1());

        private HttpClientFormula1()
        {
            HttpClientServiceCall = new HttpClientFormula1Calls();
        }

        public static HttpClientFormula1 Service => lazy.Value;
    }

}
