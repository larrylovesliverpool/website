﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Areas.Trains.Controllers
{
    public class CoachesController : Controller
    {
        // GET: Trains/Coaches
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Coaches() => View( new WebSite.ViewModels.CoachesVM().generateViewModel());
    }
}