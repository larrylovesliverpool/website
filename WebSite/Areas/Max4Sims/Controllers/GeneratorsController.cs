﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Areas.Max4Sims.Controllers
{
    public class GeneratorsController : Controller
    {
        // GET: Max4Sims/Generators
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RandomGenerator()
        {
            return View();
        }

        public ActionResult Aspirations()
        {
            return View();
        }

        public ActionResult Children()
        {
            return View();
        }
    }
}