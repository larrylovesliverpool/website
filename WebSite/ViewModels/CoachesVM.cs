﻿using System;
using System.Collections.Generic;
using WebSite.Domain;
using WebSite.Domain.DataTransferObjects;
using WebSite.ViewModels.BaseModels;

using AutoMapper;

namespace WebSite.ViewModels
{
    public class CoachesVM : ErrorCondition
    {
        public List<Domain.TrainsCoach> coaches = new List<Domain.TrainsCoach>();

        public CoachesVM generateViewModel()
        {
            BusinessLogic.TrainsCoaches coachesDetails = new BusinessLogic.TrainsCoaches();
            List<Domain.TrainsCoach> coaches = coachesDetails.read();

            coaches.ForEach(i => ToMapper(i));

            return this;
        }

        private void ToMapper(TrainsCoach coach)
        {
            coaches.Add(new TrainsCoach() {
                CoachID = coach.CoachID,
                Description = coach.Description,
                ManufacturerID = coach.ManufacturerID,
                Quantity = coach.Quantity });
        }

        public static implicit operator CoachesVM(TrainCoachesDTO coaches)
        {
            return Mapper.Map<TrainCoachesDTO>(coaches);
        }

        public static implicit operator CoachesVM(TrainCoachDTO coach)
        {
            return Mapper.Map<TrainCoachDTO>(coach);
        }

    }
}