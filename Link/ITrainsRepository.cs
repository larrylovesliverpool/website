﻿using WebSite.Domain;

namespace WebSite.ServiceInterfaces
{
    public interface ITrainsRepository : IRepositoryReader<TrainsDetail> { }
}
