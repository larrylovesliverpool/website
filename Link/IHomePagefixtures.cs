﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSite.Domain.DataTransferObjects;

namespace Link
{
    public interface IHomePagefixtures
    {
        HomePageFixtureDTO GetFixtures();
    }
}
