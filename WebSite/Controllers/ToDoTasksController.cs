﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Controllers
{
    public class ToDoTasksController : Controller
    {
        // GET: ToDoTasks
        public ActionResult Index()
        {
            ViewModels.ToDoTasksViewModel vm = new ViewModels.ToDoTasksViewModel();

            var tasks = new Respository.ToDoTasksRepository().ReadTasks();

            foreach (var task in tasks)
            {
                vm.ToDoTasks.Add(new ViewModels.ToDoTask() { Identifier = task.Identifier, Description = task.Description });
            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult DeleteTask(int taskId)
        {
            //ToDo//

            return new EmptyResult();
        }
    }
}