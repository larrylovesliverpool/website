﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain.DataTransferObjects
{

    public class FixtureDTO
    {
        private DateTime _schedule;
        private string _homeAway;
        private string _opposition;
        private bool _hasFixture;

        public DateTime Schedule
        {
            get
            {
                return _schedule;
            }

            set
            {
                _schedule = value;
            }
        }

        public string Opposition
        {
            get
            {
                return _opposition;
            }

            set
            {
                _opposition = value;
            }
        }

        public string HomeAway
        {
            get
            {
                return _homeAway;
            }

            set
            {
                _homeAway = value;
            }
        }

        public bool HasFixture
        {
            get
            {
                return _hasFixture;
            }

            set
            {
                _hasFixture = value;
            }
        }
    }

    public class FixturesDTO
    {
        public List<FixtureDTO> ListOfFixtures { get; set; }
        public string TeamName;
    }

    public class TeamsFixturesDTO
    {
        public List<FixturesDTO> ListOfTeamsFixtures { get; set; }
    }
}
