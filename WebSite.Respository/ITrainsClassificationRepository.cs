﻿using WebSite.Domain;
using WebSite.ServiceInterfaces;

namespace WebSite.Respository
{
    public interface ITrainsClassificationRepository : IRepositoryReader<TrainsClassification> { }

}
