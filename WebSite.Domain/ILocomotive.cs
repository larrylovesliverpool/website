﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSite.Domain
{
    public interface ILocomotive
    {
        string Description { get; set; }
        bool DccReady { get; set; }
        int DccNumber { get; set; }
    }
}
