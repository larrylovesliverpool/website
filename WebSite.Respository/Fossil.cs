﻿using System.Collections.Generic;
using WebSite.Domain;

namespace WebSite.Respository
{
    public class Fossil
    {
        public List<Domain.Fossil> ReadAll()
        {
            List<Domain.Fossil> ListOfFossils = new List<Domain.Fossil>();

            ListOfFossils.Add(new Domain.Fossil() { Name = "Prehistoric Rock", Rarity = "Common", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Pre-Historic Hoofprint", Rarity = "Common", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Enormous Trilobite", Rarity = "Common", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Hilariously Tiny T-Rex Arms", Rarity = "Common", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Fossilised Sim Hand", Rarity = "Common", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Fossilised Sea Monster", Rarity = "Common", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Fossilised Egg", Rarity = "Common", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Fossilised Raptor Claw", Rarity = "Common", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Fossilised Plant Imprint", Rarity = "Common", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Prehistoric Bird", Rarity = "Uncommon", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Fossilised Udder", Rarity = "Uncommon", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Perfectly Preserved Mustache", Rarity = "Uncommon", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Fossilised Whatzit", Rarity = "Rare", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Pre-Pre-Pre Sim Head", Rarity = "Rare", Comments = "" });
            ListOfFossils.Add(new Domain.Fossil() { Name = "Fossilised Alien Skull", Rarity = "Rare", Comments = "" });

            return ListOfFossils;
        }
    }
}
